import tensorflow as tf
from facegenerator.core.layers import avg_pooling
from facegenerator.core.layers import conv2d
from facegenerator.core.layers import leakyrelu, prelu
from facegenerator.core.layers import max_pooling
from facegenerator.core.layers import maxout_unit, pixelshuffle

batch_normalization = tf.layers.batch_normalization


def max_out_net(x_in, params, reuse=None):
    """
    The max out network of Anyvision
    :param x_in: 
    :param params: 
    :param reuse: 
    :return: 
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 64
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = params.strides if hasattr(params, 'strides') else (1, 1)
    padding = 'SAME'
    with tf.variable_scope('maxout', reuse=reuse):
        with tf.variable_scope('mx_color_up'):
            col_up = maxout_unit(x_in, filters=12, kernel_size=(1, 1), strides=(1, 1), padding=padding)
        with tf.variable_scope('mx_color_down'):
            col_down = maxout_unit(col_up, filters=6, kernel_size=(1, 1), strides=(1, 1), padding=padding)
        with tf.variable_scope('mx_stage_1'):
            m1 = maxout_unit(col_down, filters=base_filters, kernel_size=(5, 5), strides=(1, 1), padding=padding)
            p_max = max_pooling(m1, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            p_avg = avg_pooling(m1, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            m1_op = p_max + p_avg
        with tf.variable_scope('mx_stage_2'):
            m2 = maxout_unit(m1_op, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='1')
            m2 = maxout_unit(m2, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='2')
            m2 = maxout_unit(m2, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='3')
            m2 = maxout_unit(m2, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='4')
            p_max = max_pooling(m2, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            p_avg = avg_pooling(m2, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            m2_op = p_max + p_avg
        with tf.variable_scope('mx_stage_3'):
            m3 = maxout_unit(m2_op, filters=base_filters * 3, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='1')
            m3 = maxout_unit(m3, filters=base_filters * 3, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='2')
            m3 = maxout_unit(m3, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='3')
            m3 = maxout_unit(m3, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='4')
            m3 = maxout_unit(m3, filters=base_filters * 5, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='5')
            m3 = maxout_unit(m3, filters=base_filters * 5, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='6')
            p_max = max_pooling(m3, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            p_avg = avg_pooling(m3, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            m3_op = p_max + p_avg
        with tf.variable_scope('mx_stage_4'):
            m4 = maxout_unit(m3_op, filters=base_filters * 8, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='1')
            m4 = maxout_unit(m4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='2')
            m4 = maxout_unit(m4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='3')
            m4 = maxout_unit(m4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides, padding=padding,
                             name='4')
            p_max = max_pooling(m4, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            p_avg = avg_pooling(m4, kernel_size=(2, 2), strides=(2, 2), padding=padding)
            m4_op = p_max + p_avg
        with tf.variable_scope('mx_stage_5'):
            m5 = maxout_unit(m4_op, filters=base_filters * 10, kernel_size=(5, 5), strides=(1, 1), padding=padding,
                             name='1')
            m5 = maxout_unit(m5, filters=base_filters * 10, kernel_size=(5, 5), strides=(1, 1), padding=padding,
                             name='2')
            m_flat = tf.reshape(m5, shape=(params.batch_size, -1))
            m_op = tf.layers.dense(m_flat, units=params.num_classes, activation=None)
        return m_op


def dis_autoencoder_v1(x_in, params, reuse=None, algo='ar'):
    """

    :param x_in: 
    :param params: 
    :param reuse: 
    :return: 
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 128
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = params.strides if hasattr(params, 'strides') else (1, 1)
    padding = 'SAME'
    if algo == 'ar':
        resize_1 = [32, 32]
        resize_2 = [64, 64]
        resize_3 = [128, 128]
        resize_4 = [256, 256]
    if algo == 'sr':
        resize_1 = [12, 12]
        resize_2 = [24, 24]
        resize_3 = [48, 48]
        resize_4 = [96, 96]

    with tf.variable_scope('dis_encoder', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            x = conv2d(input_=x_in, filters=params.channels, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_2'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_3'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_4'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=(2, 2), padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_5'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_6'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_7'):
            x = conv2d(input_=x, filters=base_filters * 3, kernel_size=kernel_size, strides=(2, 2), padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_8'):
            x = conv2d(input_=x, filters=base_filters * 3, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_9'):
            x = conv2d(input_=x, filters=base_filters * 3, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
    with tf.variable_scope('dis_decoder', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_2'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
            x = tf.image.resize_nearest_neighbor(images=x, size=resize_1)
        with tf.variable_scope('conv2d_3'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_4'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
            x = tf.image.resize_nearest_neighbor(images=x, size=resize_2)
        with tf.variable_scope('conv2d_5'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_6'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
            x = tf.image.resize_nearest_neighbor(images=x, size=resize_3)
        with tf.variable_scope('conv2d_7'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_8'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
            x = tf.image.resize_nearest_neighbor(images=x, size=resize_4)
        with tf.variable_scope('conv2d_9'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_10'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_11'):
            x = conv2d(input_=x, filters=params.channels, kernel_size=kernel_size, strides=strides, padding=padding)
            x_out = tf.nn.tanh(x)
        return x, x_out


def dis_autoencoder_v2(x_in, params, training=None, reuse=None):
    """

    :param x_in:
    :param params:
    :param reuse:
    :return:
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 128
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = params.strides if hasattr(params, 'strides') else (1, 1)
    padding = 'SAME'
    with tf.variable_scope('dis_encoder', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            x = conv2d(input_=x_in, filters=params.channels, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_2'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_3'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_4'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=(2, 2), padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_5'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_6'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_7'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=(2, 2), padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_8'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_9'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
    with tf.variable_scope('dis_decoder', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_2'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.depth_to_space(x, 2)
            x = prelu(x)
        with tf.variable_scope('conv2d_4'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.depth_to_space(x, 2)
            x = prelu(x)
        with tf.variable_scope('conv2d_10'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = tf.nn.elu(x)
        with tf.variable_scope('conv2d_11'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_12'):
            x = conv2d(input_=x, filters=params.channels, kernel_size=kernel_size, strides=strides, padding=padding)
            x_out = tf.nn.tanh(x)
        return x, x_out


def dis_autoencoder_v3(x_in, params, training, reuse=None):
    """

    :param x_in:
    :param params:
    :param reuse:
    :return:
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 128
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = params.strides if hasattr(params, 'strides') else (1, 1)
    padding = 'SAME'
    with tf.variable_scope('dis_encoder', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            x = conv2d(input_=x_in, filters=params.channels, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_2'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_3'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_4'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=(2, 2), padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_5'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_6'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_7'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=(2, 2), padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_8'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_9'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
    with tf.variable_scope('dis_decoder', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_2'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_3'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = tf.depth_to_space(x, 2)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_4'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_5'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_6'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = tf.depth_to_space(x, 2)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_7'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_8'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides, padding=padding)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x)
        with tf.variable_scope('conv2d_9'):
            x = conv2d(input_=x, filters=params.channels, kernel_size=kernel_size, strides=strides, padding=padding)
            x_out = tf.nn.tanh(x)
        return x, x_out


def u_net_ar_v1(x_in, params, training, reuse=False):
    """
    The U-net architecture as explained in the paper.
    :param x_in: The input data
    :param params:
    :param reuse: True for reusing the Tensor variables.
    :return: The output of the network.
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 64
    alpha = params.alpha if hasattr(params, 'alpha') else 0.2
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = (1, 1)
    padding = 'SAME'
    with tf.variable_scope('u_net', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            xe_1 = conv2d(input_=x_in, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_1 = leakyrelu(input_=xe_1, alpha=alpha)
        with tf.variable_scope('conv2d_2'):
            xe_2 = conv2d(input_=xe_1, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_2 = batch_normalization(xe_2, training=training)
            xe_2 = leakyrelu(xe_2, alpha=alpha)
        with tf.variable_scope('conv2d_3'):
            xe_3 = conv2d(input_=xe_2, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_3 = batch_normalization(xe_3, training=training)
            xe_3 = leakyrelu(xe_3, alpha=alpha)
        with tf.variable_scope('conv2d_4'):
            xe_4 = conv2d(input_=xe_3, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_4 = batch_normalization(xe_4, training=training)
            xe_4 = leakyrelu(xe_4, alpha=alpha)
        with tf.variable_scope('conv2d_op'):
            xe_op = conv2d(input_=xe_4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                           padding=padding)
            xe_op = batch_normalization(xe_op, training=training)
            xe_op = leakyrelu(xe_op, alpha=alpha)

        # ----------------------------------------------------------------------------------------------------------

        with tf.variable_scope('conv2d_transpose_4'):
            xd_4 = conv2d(input_=xe_op, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_4 = batch_normalization(xd_4, training=training)
            xd_4 = leakyrelu(xd_4, alpha=alpha)
            xd_4 = tf.concat(values=[xd_4, xe_4], axis=-1)
        with tf.variable_scope('conv2d_transpose_3'):
            xd_3 = conv2d(input_=xd_4, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_3 = batch_normalization(xd_3, training=training)
            xd_3 = leakyrelu(xd_3)
            xd_3 = tf.concat(values=[xd_3, xe_3], axis=-1)
        with tf.variable_scope('conv2d_transpose_2'):
            xd_2 = conv2d(input_=xd_3, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_2 = batch_normalization(xd_2, training=training)
            xd_2 = leakyrelu(xd_2)
            xd_2 = tf.concat(values=[xd_2, xe_2], axis=-1)
        with tf.variable_scope('conv2d_transpose_1'):
            xd_1 = conv2d(input_=xd_2, filters=base_filters * 1, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_1 = batch_normalization(xd_1, training=training)
            xd_1 = leakyrelu(xd_1)
            xd_l = tf.concat(values=[xd_1, xe_1], axis=-1)  # xd_l to visualize the filter
        with tf.variable_scope('conv2d_transpose_0'):
            x = conv2d(input_=xd_l, filters=params.channels, kernel_size=kernel_size, strides=(1, 1),
                       padding=padding)
            x_out = tf.nn.tanh(x)
        return x, x_out


def u_net_ar_v2():
    pass


def u_net_ar_v3(x_in, params, training, reuse=False):
    """
    The U-net architecture as explained in the paper.
    :param x_in: The input data
    :param params:
    :param reuse: True for reusing the Tensor variables.
    :return: The output of the network.
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 64
    alpha = params.alpha if hasattr(params, 'alpha') else 0.2
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = (1, 1)
    padding = 'SAME'
    with tf.variable_scope('u_net', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            xe_1 = conv2d(input_=x_in, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_1 = leakyrelu(input_=xe_1, alpha=alpha)
        with tf.variable_scope('conv2d_2'):
            xe_2 = conv2d(input_=xe_1, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_2 = batch_normalization(xe_2)
            xe_2 = leakyrelu(xe_2, alpha=alpha)
        with tf.variable_scope('conv2d_3'):
            xe_3 = conv2d(input_=xe_2, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_3 = batch_normalization(xe_3)
            xe_3 = leakyrelu(xe_3, alpha=alpha)
        with tf.variable_scope('conv2d_4'):
            xe_4 = conv2d(input_=xe_3, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_4 = batch_normalization(xe_4)
            xe_4 = leakyrelu(xe_4, alpha=alpha)
        with tf.variable_scope('conv2d_op'):
            xe_op = conv2d(input_=xe_4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                           padding=padding)
            xe_op = batch_normalization(xe_op)
            xe_op = leakyrelu(xe_op, alpha=alpha)

        # ----------------------------------------------------------------------------------------------------------

        with tf.variable_scope('conv2d_dec_4'):
            xd_4 = conv2d(input_=xe_op, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_4 = batch_normalization(xd_4, training=training)
            xd_4 = leakyrelu(xd_4, alpha=alpha)
            xd_4 = tf.concat(values=[xd_4, xe_4], axis=-1)
        with tf.variable_scope('conv2d_dec_3'):
            xd_3 = conv2d(input_=xd_4, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_3 = batch_normalization(xd_3)
            xd_3 = leakyrelu(xd_3)
            xd_3 = tf.concat(values=[xd_3, xe_3], axis=-1)
        with tf.variable_scope('conv2d_dec_2'):
            xd_2 = conv2d(input_=xd_3, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_2 = batch_normalization(xd_2)
            xd_2 = leakyrelu(xd_2)
            xd_2 = tf.concat(values=[xd_2, xe_2], axis=-1)
        with tf.variable_scope('conv2d_dec_1'):
            xd_1 = conv2d(input_=xd_2, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_1 = batch_normalization(xd_1)
            xd_1 = leakyrelu(xd_1)
            xd_1 = tf.concat(values=[xd_1, xe_1], axis=-1)  # xd_l to visualize the filter
        with tf.variable_scope('final_op'):
            x = conv2d(input_=xd_1, filters=params.channels, kernel_size=kernel_size, strides=(1, 1),
                       padding=padding)
            xd_op = tf.nn.tanh(x)
        return xd_op


def u_net_sr_v1(x_in, params, reuse=False):
    """
    The U-net architecture as explained in the paper.
    :param x_in: The input data
    :param params:
    :param reuse: True for reusing the Tensor variables.
    :return: The output of the network.
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 64
    alpha = params.alpha if hasattr(params, 'alpha') else 0.2
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = (1, 1)
    padding = 'SAME'
    with tf.variable_scope('u_net', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            xe_1 = conv2d(input_=x_in, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_1 = leakyrelu(input_=xe_1, alpha=alpha)
        with tf.variable_scope('conv2d_2'):
            xe_2 = conv2d(input_=xe_1, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_2 = batch_normalization(xe_2)
            xe_2 = leakyrelu(xe_2, alpha=alpha)
        with tf.variable_scope('conv2d_3'):
            xe_3 = conv2d(input_=xe_2, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_3 = batch_normalization(xe_3)
            xe_3 = leakyrelu(xe_3, alpha=alpha)
        with tf.variable_scope('conv2d_4'):
            xe_4 = conv2d(input_=xe_3, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_4 = batch_normalization(xe_4)
            xe_4 = leakyrelu(xe_4, alpha=alpha)
        with tf.variable_scope('conv2d_op'):
            xe_op = conv2d(input_=xe_4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                           padding=padding)
            xe_op = batch_normalization(xe_op)
            xe_op = leakyrelu(xe_op, alpha=alpha)

        # ----------------------------------------------------------------------------------------------------------

        with tf.variable_scope('conv2d_dec_4'):
            xd_4 = conv2d(input_=xe_op, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_4 = tf.nn.dropout(xd_4, keep_prob=0.5)
            xd_4 = leakyrelu(xd_4)
            xd_4 = tf.concat(values=[xd_4, xe_4], axis=-1)
        with tf.variable_scope('conv2d_dec_3'):
            xd_3 = conv2d(input_=xd_4, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_3 = batch_normalization(xd_3)
            xd_3 = leakyrelu(xd_3)
            xd_3 = tf.concat(values=[xd_3, xe_3], axis=-1)
        with tf.variable_scope('conv2d_dec_2'):
            xd_2 = conv2d(input_=xd_3, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_2 = batch_normalization(xd_2)
            xd_2 = leakyrelu(xd_2)
            xd_2 = tf.concat(values=[xd_2, xe_2], axis=-1)
        with tf.variable_scope('conv2d_dec_1'):
            xd_1 = conv2d(input_=xd_2, filters=base_filters * 1, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_1 = batch_normalization(xd_1)
            xd_1 = leakyrelu(xd_1)
            xd_1 = tf.concat(values=[xd_1, xe_1], axis=-1)  # xd_l to visualize the filter
        with tf.variable_scope('pix_shuffle_0'):  # works with base filter 8
            px_0 = conv2d(input_=xd_1, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            px_0 = pixelshuffle(px_0, 2)
            px_0 = prelu(px_0)
        with tf.variable_scope('pix_shuffle_1'):  # works with base filter 8
            px_1 = conv2d(input_=px_0, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            px_1 = pixelshuffle(px_1, 2)
            px_1 = prelu(px_1)
        with tf.variable_scope('final_op'):
            x = conv2d(input_=px_1, filters=params.channels, kernel_size=kernel_size, strides=(1, 1),
                       padding=padding)
            xd_op = tf.nn.tanh(x)
        return xd_op


def u_net_sr_v2(x_in, params, reuse=False):
    """
    The U-net architecture as explained in the paper.
    :param x_in: The input data
    :param params:
    :param reuse: True for reusing the Tensor variables.
    :return: The output of the network.
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 64
    alpha = params.alpha if hasattr(params, 'alpha') else 0.2
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = (1, 1)
    padding = 'SAME'
    with tf.variable_scope('u_net', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            xe_1 = conv2d(input_=x_in, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_1 = leakyrelu(input_=xe_1, alpha=alpha)
        with tf.variable_scope('conv2d_2'):
            xe_2 = conv2d(input_=xe_1, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_2 = batch_normalization(xe_2)
            xe_2 = leakyrelu(xe_2, alpha=alpha)
        with tf.variable_scope('conv2d_3'):
            xe_3 = conv2d(input_=xe_2, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_3 = batch_normalization(xe_3)
            xe_3 = leakyrelu(xe_3, alpha=alpha)
        with tf.variable_scope('conv2d_4'):
            xe_4 = conv2d(input_=xe_3, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_4 = batch_normalization(xe_4)
            xe_4 = leakyrelu(xe_4, alpha=alpha)
        with tf.variable_scope('conv2d_op'):
            xe_op = conv2d(input_=xe_4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                           padding=padding)
            xe_op = batch_normalization(xe_op)
            xe_op = leakyrelu(xe_op, alpha=alpha)

        # ----------------------------------------------------------------------------------------------------------

        with tf.variable_scope('conv2d_dec_4'):
            xd_4 = conv2d(input_=xe_op, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_4 = tf.nn.dropout(xd_4, keep_prob=0.5)
            xd_4 = leakyrelu(xd_4)
            xd_4 = tf.concat(values=[xd_4, xe_4], axis=-1)
        with tf.variable_scope('conv2d_dec_3'):
            xd_3 = conv2d(input_=xd_4, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_3 = batch_normalization(xd_3)
            xd_3 = leakyrelu(xd_3)
            xd_3 = tf.concat(values=[xd_3, xe_3], axis=-1)
        with tf.variable_scope('conv2d_dec_2'):
            xd_2 = conv2d(input_=xd_3, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_2 = batch_normalization(xd_2)
            xd_2 = leakyrelu(xd_2)
            xd_2 = tf.concat(values=[xd_2, xe_2], axis=-1)
        with tf.variable_scope('conv2d_dec_1'):
            xd_1 = conv2d(input_=xd_2, filters=base_filters * 1, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_1 = batch_normalization(xd_1)
            xd_1 = leakyrelu(xd_1)
            xd_1 = tf.concat(values=[xd_1, xe_1], axis=-1)  # xd_l to visualize the filter
        with tf.variable_scope('pix_shuffle_0'):  # works with base filter 8
            px_0 = conv2d(input_=xd_1, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            px_0 = pixelshuffle(px_0, 2)
            px_0 = prelu(px_0)
        with tf.variable_scope('pix_shuffle_1'):  # works with base filter 8
            px_1 = conv2d(input_=px_0, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            px_1 = pixelshuffle(px_1, 2)
            px_1 = prelu(px_1)
        with tf.variable_scope('conv2d_dec_5'):
            xd_5 = conv2d(input_=px_1, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_5 = batch_normalization(xd_5)
            xd_5 = leakyrelu(xd_5)
        with tf.variable_scope('conv2d_dec_6'):
            xd_6 = conv2d(input_=xd_5, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_6 = batch_normalization(xd_6)
            xd_6 = leakyrelu(xd_6)
        with tf.variable_scope('final_op'):
            x = conv2d(input_=xd_6, filters=params.channels, kernel_size=kernel_size, strides=(1, 1),
                       padding=padding)
            xd_op = tf.nn.tanh(x)
        return xd_op


def u_net_sr_v3(x_in, params, reuse=False):
    """
    The U-net architecture as explained in the paper.
    :param x_in: The input data
    :param params:
    :param reuse: True for reusing the Tensor variables.
    :return: The output of the network.
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 64
    alpha = params.alpha if hasattr(params, 'alpha') else 0.2
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = (1, 1)
    padding = 'SAME'
    with tf.variable_scope('u_net', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            xe_1 = conv2d(input_=x_in, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_1 = leakyrelu(input_=xe_1, alpha=alpha)
        with tf.variable_scope('conv2d_2'):
            xe_2 = conv2d(input_=xe_1, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_2 = batch_normalization(xe_2)
            xe_2 = leakyrelu(xe_2, alpha=alpha)
        with tf.variable_scope('conv2d_3'):
            xe_3 = conv2d(input_=xe_2, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_3 = batch_normalization(xe_3)
            xe_3 = leakyrelu(xe_3, alpha=alpha)
        with tf.variable_scope('conv2d_4'):
            xe_4 = conv2d(input_=xe_3, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_4 = batch_normalization(xe_4)
            xe_4 = leakyrelu(xe_4, alpha=alpha)
        with tf.variable_scope('conv2d_op'):
            xe_op = conv2d(input_=xe_4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                           padding=padding)
            xe_op = batch_normalization(xe_op)
            xe_op = leakyrelu(xe_op, alpha=alpha)

        # ----------------------------------------------------------------------------------------------------------

        with tf.variable_scope('conv2d_dec_4'):
            xd_4 = conv2d(input_=xe_op, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_4 = tf.nn.dropout(xd_4, keep_prob=0.5)
            xd_4 = leakyrelu(xd_4)
            xd_4 = tf.concat(values=[xd_4, xe_4], axis=-1)
        with tf.variable_scope('conv2d_dec_3'):
            xd_3 = conv2d(input_=xd_4, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_3 = batch_normalization(xd_3)
            xd_3 = leakyrelu(xd_3)
            xd_3 = tf.concat(values=[xd_3, xe_3], axis=-1)
        with tf.variable_scope('conv2d_dec_2'):
            xd_2 = conv2d(input_=xd_3, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_2 = batch_normalization(xd_2)
            xd_2 = leakyrelu(xd_2)
            xd_2 = tf.concat(values=[xd_2, xe_2], axis=-1)
        with tf.variable_scope('conv2d_dec_1'):
            xd_1 = conv2d(input_=xd_2, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_1 = batch_normalization(xd_1)
            xd_1 = leakyrelu(xd_1)
            xd_1 = tf.concat(values=[xd_1, xe_1], axis=-1)  # xd_l to visualize the filter
        with tf.variable_scope('pix_shuffle_0'):  # works with base filter 8
            px_0 = conv2d(input_=xd_1, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            px_0 = batch_normalization(px_0)
            px_0 = pixelshuffle(px_0, 2)
            px_0 = prelu(px_0)
        with tf.variable_scope('pix_shuffle_1'):  # works with base filter 8
            px_1 = conv2d(input_=px_0, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            px_1 = batch_normalization(px_1)
            px_1 = pixelshuffle(px_1, 2)
            px_1 = prelu(px_1)
        with tf.variable_scope('final_op'):
            x = conv2d(input_=px_1, filters=params.channels, kernel_size=kernel_size, strides=(1, 1),
                       padding=padding)
            xd_op = tf.nn.tanh(x)
        return xd_op


def u_net_sr_v4(x_in, params, reuse=False):
    """
    The U-net architecture as explained in the paper.
    :param x_in: The input data
    :param params:
    :param reuse: True for reusing the Tensor variables.
    :return: The output of the network.
    """
    base_filters = params.base_filters if hasattr(params, 'base_filters') else 64
    alpha = params.alpha if hasattr(params, 'alpha') else 0.2
    kernel_size = params.kernel_size if hasattr(params, 'kernel_size') else (3, 3)
    strides = (1, 1)
    padding = 'SAME'
    with tf.variable_scope('u_net', reuse=reuse):
        with tf.variable_scope('conv2d_1'):
            xe_1 = conv2d(input_=x_in, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_1 = batch_normalization(xe_1)
            xe_1 = pixelshuffle(xe_1, 2)
            xe_1 = prelu(xe_1)
        with tf.variable_scope('conv2d_2'):
            xe_2 = conv2d(input_=xe_1, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_2 = batch_normalization(xe_2)
            xe_2 = leakyrelu(xe_2, alpha=alpha)
        with tf.variable_scope('conv2d_3'):
            xe_3 = conv2d(input_=xe_2, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_3 = batch_normalization(xe_3)
            xe_3 = leakyrelu(xe_3, alpha=alpha)
        with tf.variable_scope('conv2d_4'):
            xe_4 = conv2d(input_=xe_3, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xe_4 = batch_normalization(xe_4)
            xe_4 = leakyrelu(xe_4, alpha=alpha)
        with tf.variable_scope('conv2d_op'):
            xe_op = conv2d(input_=xe_4, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                           padding=padding)
            xe_op = batch_normalization(xe_op)
            xe_op = leakyrelu(xe_op, alpha=alpha)

        # ----------------------------------------------------------------------------------------------------------

        with tf.variable_scope('conv2d_dec_4'):
            xd_4 = conv2d(input_=xe_op, filters=base_filters * 8, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_4 = tf.nn.dropout(xd_4, keep_prob=0.5)
            xd_4 = leakyrelu(xd_4)
            xd_4 = tf.concat(values=[xd_4, xe_4], axis=-1)
        with tf.variable_scope('conv2d_dec_3'):
            xd_3 = conv2d(input_=xd_4, filters=base_filters * 4, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_3 = batch_normalization(xd_3)
            xd_3 = leakyrelu(xd_3)
            xd_3 = tf.concat(values=[xd_3, xe_3], axis=-1)
        with tf.variable_scope('conv2d_dec_2'):
            xd_2 = conv2d(input_=xd_3, filters=base_filters * 2, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_2 = batch_normalization(xd_2)
            xd_2 = leakyrelu(xd_2)
            xd_2 = tf.concat(values=[xd_2, xe_2], axis=-1)
        with tf.variable_scope('conv2d_dec_1'):
            xd_1 = conv2d(input_=xd_2, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            xd_1 = batch_normalization(xd_1)
            xd_1 = leakyrelu(xd_1)
            xd_1 = tf.concat(values=[xd_1, xe_1], axis=-1)  # xd_l to visualize the filter
        # with tf.variable_scope('pix_shuffle_0'):  # works with base filter 8
        #     px_0 = conv2d(input_=xd_1, filters=base_filters, kernel_size=kernel_size, strides=strides,
        #                   padding=padding)
        #     px_0 = batch_normalization(px_0)
        #     px_0 = pixelshuffle(px_0, 2)
        #     px_0 = prelu(px_0)
        with tf.variable_scope('pix_shuffle_1'):  # works with base filter 8
            px_1 = conv2d(input_=xd_1, filters=base_filters, kernel_size=kernel_size, strides=strides,
                          padding=padding)
            px_1 = batch_normalization(px_1)
            px_1 = pixelshuffle(px_1, 2)
            px_1 = prelu(px_1)
        with tf.variable_scope('final_op'):
            x = conv2d(input_=px_1, filters=params.channels, kernel_size=(9, 9), strides=(1, 1),
                       padding=padding)
            xd_op = tf.nn.tanh(x)
        return xd_op


def discriminator_v2(x_in, params, reuse=None, training=True):
    """
    The discriminator of the GAN
    :param x_in:
    :param reuse:
    :return: The output of the generator.
    """
    base_filters = 64
    kernel_size = (3, 3)
    strides1 = (1, 1)
    strides2 = (2, 2)
    alpha = 0.2
    pad = 'SAME'
    with tf.variable_scope('discriminator_v2', reuse=reuse):
        with tf.variable_scope('conv2d_in'):
            x = conv2d(input_=x_in, filters=base_filters, kernel_size=(9, 9), strides=strides1, padding=pad)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('block_1'):
            x = conv2d(input_=x, filters=base_filters, kernel_size=kernel_size, strides=strides2, padding=pad)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('block_2'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides1, padding=pad)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('block_3'):
            x = conv2d(input_=x, filters=base_filters * 2, kernel_size=kernel_size, strides=strides2, padding=pad)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('block_4'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides1, padding=pad)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('block_5'):
            x = conv2d(input_=x, filters=base_filters * 4, kernel_size=kernel_size, strides=strides2, padding=pad)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('block_6'):
            x = conv2d(input_=x, filters=base_filters * 8, kernel_size=kernel_size, strides=strides1, padding=pad)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('block_7'):
            x = conv2d(input_=x, filters=base_filters * 8, kernel_size=kernel_size, strides=strides2, padding=pad)
            x = batch_normalization(x, training=training)
            x = leakyrelu(x, alpha)

        with tf.variable_scope('dense_1024'):
            x = tf.reshape(x, shape=(params.batch_size, -1))
            x = tf.layers.dense(x, units=1024, activation=None)
            x = leakyrelu(x, alpha)
        with tf.variable_scope('dense_1'):
            x = tf.layers.dense(x, units=1, activation=None)
            x_out = tf.sigmoid(x, name=None)
        return x_out


class Params(object):
    def __init__(self, **kwargs):
        """
        Object to specify the parameters
        :param kwargs: alpha - parameter of leaky relu
        """
        allowed_kwargs = {'base_filters',
                          'kernel_size',
                          'strides',
                          'dim_encoding',
                          'num_classes',
                          'channels',
                          'batch_size',
                          'alpha'}
        for k in kwargs:
            if k not in allowed_kwargs:
                raise TypeError('Unexpected keyword argument passed: ' + str(k))
        self.__dict__.update(kwargs)


if __name__ == '__main__':
    pass
