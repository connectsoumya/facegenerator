from __future__ import print_function
import errno
import os
import sys
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from PIL import Image
from tensorflow import errors
from facegenerator.utils import DataGenerator
from facegenerator.utils.visualize import plot_data, save_image
from facegenerator.utils.visualize import plot_generated
from facegenerator.utils.loss_functions import mse_loss, l1_loss, exp_loss
from facegenerator.utils.metrics import psnr
from facegenerator.utils.metrics import ssim, gmsd
from facegenerator.utils._vgg19 import vgg19_simple_api
from facegenerator.utils.misc import is_square
from facegenerator.utils.metrics import haarpsi
from facegenerator.core.etc import save
from facegenerator.core.etc import load
from facegenerator.core.models import u_net_sr_v4
from facegenerator.core.models import dis_autoencoder_v3
from facegenerator.core.models import u_net_sr_v3
from facegenerator.core.models import dis_autoencoder_v2
from facegenerator.core.models import dis_autoencoder_v1
from facegenerator.core.models import u_net_sr_v1
from facegenerator.core.models import Params
import matplotlib.pyplot as plt
from functools import reduce
import tensorlayer as tl
import logging
import logging.config

if sys.version_info.major == 2:
    from ConfigParser import SafeConfigParser as Parser
else:
    from configparser import ConfigParser as Parser

parser = Parser()
my_path = os.path.abspath(os.path.dirname(__file__))
log_file_path = os.path.join(my_path, '../../logging.ini')
config_file_path = os.path.join(my_path, '../../config.ini')
parser.read(config_file_path)
logging.config.fileConfig(log_file_path)


def rgb2gray(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray


class SuperResolution(object):
    def __init__(self, batch_size, learning_rate, epoch, **kwargs):
        allowed_kwargs = {'check_every',
                          'channels',
                          'img_save_path',
                          'model_save_path',
                          'test_path',
                          'GPU_list'}
        for k in kwargs:
            if k not in allowed_kwargs:
                raise TypeError('Unexpected keyword argument passed: ' + str(k))
        self.channels = 3
        self.img_save_path = '~/Pictures/'
        self.model_save_path = '~/Documents/'
        self.test_path = None
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.epoch = epoch
        self.training_loss = {'gmsd': [], 'haarpsi': []}
        self.k_t = 0
        self.kappa = 1.0
        self.alpha = 0.5
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        self.__dict__.update(kwargs)

    def generator(self, x_in, reuse=False):
        """
        The generator of the GAN
        :param x_in:
        :param reuse:
        :return: The output of the generator.
        """
        kwargs = {'base_filters': 32,
                  'kernel_size': (3, 3),
                  'strides': (1, 1),
                  'channels': self.channels,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('generator', reuse=reuse):
            logging.info('U-Net generator in action')
            x = u_net_sr_v4(x_in, params=u_net_params, reuse=reuse)
            return x

    def discriminator(self, x_in, reuse=None):
        kwargs = {'base_filters': 32,
                  'kernel_size': (3, 3),
                  'strides': (1, 1),
                  'channels': self.channels,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('discriminator', reuse=reuse):
            x, x_out = dis_autoencoder_v3(x_in, params=u_net_params, training=True, reuse=reuse)
            return x, x_out

    def fit_graph(self, tr_path):
        gamma = self.gamma if hasattr(self, 'gamma') else 0.9
        channels = self.channels
        GPU = self.GPU_list if hasattr(self, 'GPU_list') else ['/gpu:0']
        self.num_gpu = len(GPU)
        logging.info(str(self.num_gpu) + ' GPUs found')
        print('Working with ' + str(self.num_gpu) + ' GPUs')
        kwargs = {'data_resolution': (24, 24),
                  'gt_resolution': (96, 96)}
        gen = DataGenerator(tr_path, **kwargs)
        d_loss = []
        g_loss = []
        fake_image_tmp = []
        self.training_image = tf.placeholder(dtype=tf.float32, shape=(None, None, None, channels),
                                             name='gen_input')
        self.ground_truth = tf.placeholder(dtype=tf.float32, shape=(None, None, None, channels), name='gnd_truth')
        tr_im = tf.split(self.training_image, self.num_gpu)
        gt_im = tf.split(self.ground_truth, self.num_gpu)
        gpu_id = 0
        logging.debug('Placeholders created and splitted')

        def vggloss_func(list1, list2):
            return tf.norm(list1.outputs - list2.outputs)

        for d in GPU:
            with tf.device(d):
                fake_image = self.generator(tr_im[gpu_id], reuse=tf.AUTO_REUSE)
                d_real_logit, discriminator_output_real = self.discriminator(gt_im[gpu_id], reuse=tf.AUTO_REUSE)
                d_fake_logit, discriminator_output_fake = self.discriminator(fake_image, reuse=True)
                loss_real = l1_loss(discriminator_output_real, gt_im[gpu_id])
                loss_fake = l1_loss(discriminator_output_fake, fake_image)
                # ======================= VGG LOSS======================
                self.net_vgg, vgg_target_emb = vgg19_simple_api((gt_im[gpu_id] + 1) / 2, reuse=tf.AUTO_REUSE)
                _, vgg_predict_emb = vgg19_simple_api((fake_image + 1) / 2, reuse=True)
                vgg_loss = vggloss_func(vgg_predict_emb[1], vgg_target_emb[1])
                # =======================================================
                d_loss.append(l1_loss(loss_real, self.k_t * loss_fake * 10e-3))
                g_loss.append(vgg_loss + l1_loss(vgg_predict_emb[1].outputs, vgg_target_emb[1].outputs))
                logging.debug('Model for GPU %s created' % d)
                gpu_id += 1
            fake_image_tmp.append(fake_image)
        self.fake_image = tf.concat(fake_image_tmp, axis=0)
        self.d_loss = tf.reduce_mean(d_loss, axis=0)
        self.g_loss = tf.reduce_mean(g_loss, axis=0)
        trainable_vars = tf.trainable_variables()
        dcr_vars = [var for var in trainable_vars if 'discriminator' in var.name]
        gen_vars = [var for var in trainable_vars if 'generator' in var.name]
        self.opt_g = self.optimizer.minimize(loss=self.g_loss, var_list=gen_vars, colocate_gradients_with_ops=True)
        self.opt_d = self.optimizer.minimize(loss=self.d_loss, var_list=dcr_vars + gen_vars,
                                             colocate_gradients_with_ops=True)
        self.k_t = tf.reduce_min([tf.reduce_max([self.k_t + np.abs(gamma * loss_real - loss_fake), 0.]), 1.])
        self.train(gen)

    def train(self, data_generator):
        check_every = self.check_every if hasattr(self, 'check_every') else 500
        counter = 0
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            # ===== LOAD VGG =====
            vgg19_npy_path = os.path.join(my_path, '../../model/VGG19/vgg19.npy')
            print('VGG19 model path ' + vgg19_npy_path)
            if not os.path.isfile(vgg19_npy_path):
                print('Please download vgg19.npz from : https://github.com/machrisaa/tensorflow-vgg')
                exit()
            npz = np.load(vgg19_npy_path, encoding='latin1').item()
            params = []
            for val in sorted(npz.items()):
                W = np.asarray(val[1][0])
                b = np.asarray(val[1][1])
                print("  Loading %s: %s, %s" % (val[0], W.shape, b.shape))
                params.extend([W, b])
            tl.files.assign_params(sess, params[:24], self.net_vgg)
            # ===== LOAD MODEL =====
            try:
                model_path = os.path.join(my_path, '../../model/weights.mdl')
                load(sess, filepath=model_path)
                print('Previous model loaded from %s' % (model_path))
            except errors.NotFoundError:
                logging.info('No models found, training from start')
            except errors.InvalidArgumentError as e:
                print(str(e))
                logging.info('Model of different architecture found. Need to clean it up before proceeding')
                raise IOError('Model of different architecture found. Remove it before proceeding')
            try:
                for epoch in tqdm(range(self.epoch)):
                    # for training_batch, gt_batch in data_generator.yield_arsr_batch(self.batch_size):
                    for training_batch, gt_batch in data_generator.yield_sr_batch_v2(self.batch_size):
                        # convert the data for tanh range
                        gt_batch = gt_batch * 2 - 1
                        training_batch = training_batch * 2 - 1
                        counter += 1
                        print('\r batches: {} epochs: {}'.format(counter, epoch), end="")
                        # logging.debug('batches: {} epochs: {}'.format(counter, epoch))
                        if np.mod(counter, check_every) != 0:

                            _, gen_loss = sess.run(fetches=[self.opt_g, self.g_loss],
                                                   feed_dict={self.training_image: training_batch,
                                                              self.ground_truth: gt_batch})
                            _, dis_loss, k_t = sess.run(
                                fetches=[self.opt_d, self.d_loss, self.k_t],
                                feed_dict={self.training_image: training_batch,
                                           self.ground_truth: gt_batch})
                        else:
                            # plot the data
                            img = sess.run(fetches=self.fake_image, feed_dict={self.training_image: training_batch})
                            img = (img + 1) / 2.0
                            gt = (gt_batch + 1) / 2.0
                            psnr_ = psnr(gt.astype(np.float32), img)
                            ssim_ = ssim(gt.astype(np.float32), img, multichannel=True)
                            gmsd_ = gmsd(gt, img)
                            haarpsi_ = haarpsi(gt, img, True)
                            # plot images from the test set if test path is provided ----------------
                            if self.test_path is not None:
                                gen = DataGenerator(self.test_path)
                                for img_batch_raw, filename in gen.yield_predict_batch_sr(self.num_gpu):
                                    img_batch = img_batch_raw * 2 - 1
                                    img = sess.run(fetches=(self.fake_image),
                                                   feed_dict={self.training_image: img_batch})
                                    pred_img = img[0, :, :, :]
                                    pred_img = (pred_img + 1) / 2.0
                                    for i in range(img_batch.shape[0]):
                                        save_image((pred_img * 255).astype(np.uint8),
                                                   self.img_save_path + filename.replace('.jpg', ''))
                            # -----------------------------------------------------------------------
                            print('PSNR: %f, SSIM: %f, GMSD: %f, HaarPSI: %f' % (psnr_, ssim_, gmsd_, haarpsi_))
                            logging.debug('%d epochs completed' % epoch)
                            save(sess, self.model_save_path + 'weights.mdl')
                            logging.info('Saved model successfully')
                            counter = 0
            except KeyboardInterrupt:
                logging.info('Early break of loop due to keyboard interruption.')
                save(sess, self.model_save_path)
                logging.info('Saved model successfully')
                sess.close()
        logging.info('Training completed.')

    def predict(self, cond_img_path):
        """
        Predict the output of a full image of any size.
        :param cond_img_path: 
        :return: 
        """
        gen = DataGenerator(cond_img_path)
        self.input_image = tf.placeholder(dtype=tf.float32, shape=(None, None, None, 3),
                                          name='gen_input')
        self.fake_image = self.generator(self.input_image)
        with tf.Session() as sess:
            try:
                load(sess, filepath=self.model_save_path + 'weights.mdl')
            except errors.NotFoundError as e:
                print(str(e))
                logging.info('No models found. Aborting...')
                raise IOError('No models found. Aborting...')
            except errors.InvalidArgumentError as e:
                logging.info('Model of different architecture found. Aborting...')
                print(str(e))
                raise IOError('Model of different architecture found. Aborting...')
            for img_batch_raw, filename in gen.yield_predict_batch_sr(1):
                img_batch = img_batch_raw * 2 - 1
                img = sess.run(fetches=(self.fake_image), feed_dict={self.input_image: img_batch})
                pred_img = img[0, :, :, :]
                pred_img = (pred_img + 1) / 2.0
                save_image((pred_img * 255).astype(np.uint8), self.img_save_path + filename.replace('.jpg', ''))

    def visualize_filters(self, ip, img=0):
        filters = int(ip.shape[-1])
        row, col = self.factors(filters)
        plt.subplots(nrows=row, ncols=col)
        for i in range(filters):
            plt.subplot(row, col, i + 1)
            plt.imshow(ip[img, :, :, i], cmap='hot', interpolation='nearest')
        plt.show()

    @staticmethod
    def factors(n):
        if is_square(n):
            return int(np.sqrt(n)), int(np.sqrt(n))
        fac = set(reduce(list.__add__, ([i, n // i] for i in range(1, int(pow(n, 0.5) + 1)) if n % i == 0)))
        lim = np.sqrt(n)
        fac_1 = []
        fac_2 = []
        for f in fac:
            if f < lim:
                fac_1.append(f)
            else:
                fac_2.append(f)
        return (max(fac_1), min(fac_2))


if __name__ == '__main__':
    tr_path_ = parser.get('DataGeneratorIMNETSR', 'tr')
    test_path = parser.get('DataGeneratorIMNETSR', 'test_data_path')
    test_path_gt = parser.get('DataGeneratorIMNETSR', 'test_data_path_gt')
    GPU = parser.get('main', 'GPU').split()
    batch_size_ = int(parser.get('main', 'batch_size'))
    check_every_ = int(parser.get('main', 'check_every'))
    gen_img_save_path = parser.get('main', 'save_path')
    f = SuperResolution(batch_size_, 0.001, 100, check_every=check_every_, channels=3, )
    f.fit_graph(tr_path_)
    # f.predict(test_path)
