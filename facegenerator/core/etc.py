import tensorflow as tf
import os
import logging
import logging.config

my_path = os.path.abspath(os.path.dirname(__file__))
log_file_path = os.path.join(my_path, '../../logging.ini')
logging.config.fileConfig(log_file_path)


def save(sess, filepath='../tmp/tfmodel.mdl', global_step=None):
    """
    Save a TensorFlow model.
    :param sess:
    :param filepath:
    :param global_step:
    :return:
    """
    saver = tf.train.Saver()
    saver.save(sess, filepath, global_step=global_step)
    logging.info('Model saved at ' + filepath)


def load(sess, filepath):
    """
    Load/Restore a TensorFlow model.
    :param sess: The session
    :param filepath:
    :return:
    """
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    saver = tf.train.Saver()
    saver.restore(sess, filepath)
    logging.info('Model restored from ' + filepath)
    return sess
