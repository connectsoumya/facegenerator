from __future__ import print_function
import errno
import os
import sys
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from tensorflow import errors
from facegenerator.utils import DataGenerator
from facegenerator.utils.visualize import save_image
from facegenerator.utils.loss_functions import canny_vgg_loss
from facegenerator.utils.misc import average_gradients
from facegenerator.utils.metrics import psnr
from facegenerator.utils.metrics import ssim, gmsd
from facegenerator.utils.metrics import haarpsi
from facegenerator.utils.misc import is_square
from facegenerator.core.models import u_net_ar_v1
from facegenerator.core.models import dis_autoencoder_v3
from facegenerator.core.etc import save
from facegenerator.core.etc import load
from facegenerator.core.models import Params
import matplotlib.pyplot as plt
from functools import reduce
import tensorlayer as tl
import logging
import logging.config

if sys.version_info.major == 2:
    from ConfigParser import SafeConfigParser as Parser
else:
    from configparser import ConfigParser as Parser

parser = Parser()
my_path = os.path.abspath(os.path.dirname(__file__))
log_file_path = os.path.join(my_path, '../../logging.ini')
config_file_path = os.path.join(my_path, '../../config.ini')
parser.read(config_file_path)
logging.config.fileConfig(log_file_path)


def rgb2gray(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray


class ArtifactRemoval(object):
    def __init__(self, batch_size, learning_rate, epoch, **kwargs):
        allowed_kwargs = {'check_every',
                          'channels',
                          'img_save_path',
                          'model_save_path',
                          'test_path',
                          'GPU_list'}
        for k in kwargs:
            if k not in allowed_kwargs:
                raise TypeError('Unexpected keyword argument passed: ' + str(k))
        self.channels = 3
        self.img_save_path = '~/Pictures/'
        self.model_save_path = '~/Documents/'
        self.test_path = None
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.epoch = epoch
        self.training_loss = {'gmsd': [], 'haarpsi': []}
        self.k = 0.0
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        self.__dict__.update(kwargs)

    def generator(self, x_in, training, reuse=False):
        """
        The generator of the GAN
        :param x_in:
        :param reuse:
        :return: The output of the generator.
        """
        kwargs = {'base_filters': 32,
                  'kernel_size': (3, 3),
                  'strides': (1, 1),
                  'channels': self.channels,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('generator', reuse=reuse):
            logging.info('U-Net generator in action')
            x_logit, x_out = u_net_ar_v1(x_in, params=u_net_params, reuse=reuse, training=training)
            return x_logit, x_out

    def discriminator(self, x_in, training, reuse=None):
        kwargs = {'base_filters': 32,
                  'kernel_size': (3, 3),
                  'strides': (1, 1),
                  'channels': self.channels,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('discriminator', reuse=reuse):
            x_logit, x_out = dis_autoencoder_v3(x_in, params=u_net_params, training=training, reuse=reuse)
            return x_logit, x_out

    def fit_graph(self, tr_path):
        gamma = self.gamma if hasattr(self, 'gamma') else 0.9
        channels = self.channels
        GPU = self.GPU_list if hasattr(self, 'GPU_list') else ['/gpu:0']
        self.num_gpu = len(GPU)
        kwargs = {'data_resolution': (256, 256),
                  'gt_resolution': (256, 256)}
        gen = DataGenerator(tr_path, **kwargs)
        fake_image_tmp = []
        tower_grads_g = []
        tower_grads_d = []
        self.training_image = tf.placeholder(dtype=tf.float32, shape=(None, None, None, channels), name='gen_input')
        self.ground_truth = tf.placeholder(dtype=tf.float32, shape=(None, None, None, channels), name='gnd_truth')
        tr_im = tf.split(self.training_image, self.num_gpu)
        gt_im = tf.split(self.ground_truth, self.num_gpu)
        gpu_id = 0
        logging.debug('Placeholders created and splitted')

        for d in GPU:
            with tf.device(d):
                # Get the tower outputs from the network
                g_fake_logit, g_fake_image = self.generator(tr_im[gpu_id], training=True, reuse=tf.AUTO_REUSE)
                d_real_logit, d_real_output = self.discriminator(gt_im[gpu_id], training=True, reuse=tf.AUTO_REUSE)
                d_fake_logit, d_fake_output = self.discriminator(g_fake_image, training=True, reuse=True)
                # Get the tower losses
                self.net_vgg, d_loss_fake = canny_vgg_loss(d_fake_output, g_fake_image)
                _, d_loss_real = canny_vgg_loss(d_real_output, gt_im[gpu_id])
                _, self.g_loss = canny_vgg_loss(g_fake_image, gt_im[gpu_id])
                self.d_loss = tf.abs(d_loss_real - self.k * d_loss_fake)
                # Get the trainable variables of the generator and discriminator
                trainable_vars = tf.trainable_variables()
                dcr_vars = [var for var in trainable_vars if 'discriminator' in var.name]
                gen_vars = [var for var in trainable_vars if 'generator' in var.name]
                # Compute the tower gradients
                tower_grads_g.append(self.optimizer.compute_gradients(loss=self.g_loss, var_list=gen_vars))
                tower_grads_d.append(self.optimizer.compute_gradients(loss=self.d_loss, var_list=gen_vars + dcr_vars))
                # Get the fake image from each tower
                fake_image_tmp.append(g_fake_image)
            logging.debug('Model for GPU %s created' % d)
            gpu_id += 1
        # Get the average of all the tower gradients
        grad_g = average_gradients(tower_grads_g)
        grad_d = average_gradients(tower_grads_d)
        # Apply the gradients to the model
        self.opt_g = self.optimizer.apply_gradients(grad_g)
        self.opt_d = self.optimizer.apply_gradients(grad_d)
        # Other required computations
        self.fake_image = tf.concat(fake_image_tmp, axis=0)
        self.k = tf.reduce_min([tf.reduce_max([self.k + 10e-3 * (gamma * d_loss_real - d_loss_fake), 0.]), 1.])
        self.train(gen)

    def train(self, data_generator):
        global save_path
        check_every = self.check_every if hasattr(self, 'check_every') else 500
        counter = 0
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            # ===== LOAD VGG =====
            vgg19_npy_path = os.path.join(my_path, '../../model/VGG19/vgg19.npy')
            print('VGG19 model path ' + vgg19_npy_path)
            if not os.path.isfile(vgg19_npy_path):
                print('Please download vgg19.npz from : https://github.com/machrisaa/tensorflow-vgg')
                exit()
            npz = np.load(vgg19_npy_path, encoding='latin1').item()
            params = []
            for val in sorted(npz.items()):
                W = np.asarray(val[1][0])
                b = np.asarray(val[1][1])
                print("  Loading %s: %s, %s" % (val[0], W.shape, b.shape))
                params.extend([W, b])
            tl.files.assign_params(sess, params[:24], self.net_vgg)
            # ===== LOAD MODEL =====
            try:
                load(sess, filepath='../../model/VGG19/weights.mdl')
                print('Previous model loaded from %s' % '../../model/VGG19/weights.mdl')
            except errors.NotFoundError as e:
                logging.info(str(e) + 'No models found. Training from scratch...\n')
            except errors.InvalidArgumentError as e:
                logging.info('Model of different architecture found. Aborting...\n' + str(e))
                # raise IOError('Model of different architecture found. Aborting...\n' + str(e))
            try:
                for epoch in tqdm(range(self.epoch)):
                    # for training_batch, gt_batch in data_generator.yield_arsr_batch(self.batch_size):
                    for training_batch, gt_batch in data_generator.yield_ar_batch_v1(self.batch_size):
                        # convert the data for tanh range
                        gt_batch = gt_batch * 2 - 1
                        training_batch = training_batch * 2 - 1
                        counter += 1
                        print('\r batches: {} epochs: {}'.format(counter, epoch), end="")
                        logging.debug('batches: {} epochs: {}'.format(counter, epoch))
                        if np.mod(counter, check_every) != 0:

                            _, gen_loss = sess.run(fetches=[self.opt_g, self.g_loss],
                                                   feed_dict={self.training_image: training_batch,
                                                              self.ground_truth: gt_batch})
                            _, dis_loss, k_t = sess.run(
                                fetches=[self.opt_d, self.d_loss, self.k],
                                feed_dict={self.training_image: training_batch,
                                           self.ground_truth: gt_batch})
                        else:
                            # plot the data
                            img = sess.run(fetches=(self.fake_image), feed_dict={self.training_image: training_batch})
                            img = (img + 1) / 2.0
                            gt = (gt_batch + 1) / 2.0
                            psnr_ = psnr(gt.astype(np.float32), img)
                            ssim_ = ssim(gt.astype(np.float32), img, multichannel=True)
                            gmsd_ = gmsd(gt, img)
                            haarpsi_ = haarpsi(gt, img, True)
                            # plot images from the test set if test path is provided ----------------
                            if self.test_path is not None:
                                gen = DataGenerator(self.test_path)
                                for img_batch_raw, filename in gen.yield_predict_batch_ar(self.num_gpu):
                                    img_batch = img_batch_raw * 2 - 1
                                    img = sess.run(fetches=(self.fake_image),
                                                   feed_dict={self.training_image: img_batch})
                                    pred_img = img[0, :, :, :]
                                    pred_img = (pred_img + 1) / 2.0
                                    for i in range(img_batch.shape[0]):
                                        if pred_img.shape[-1] == 1:
                                            save_image((pred_img[:, :, 0] * 255).astype(np.uint8),
                                                       self.img_save_path + filename.replace('.jpg', ''))
                            # -----------------------------------------------------------------------
                            print('PSNR: %f, SSIM: %f, GMSD: %f, HaarPSI: %f' % (psnr_, ssim_, gmsd_, haarpsi_))
                            logging.debug('%d epochs completed' % epoch)
                            save(sess, self.model_save_path + 'weights.mdl')
                            logging.info('Saved model successfully')
                            counter = 0
            except KeyboardInterrupt:
                logging.info('Early break of loop due to keyboard interruption.')
                save(sess, self.model_save_path)
                logging.info('Saved model successfully')
                sess.close()
        logging.info('Training completed.')

    def predict(self, cond_img_path):
        """
        Predict the output of a full image of any size.
        :param cond_img_path: 
        :return: 
        """
        gen = DataGenerator(cond_img_path)
        self.input_image = tf.placeholder(dtype=tf.float32, shape=(None, None, None, 1),
                                          name='gen_input')
        _, self.fake_image = self.generator(self.input_image, training=True)
        with tf.Session() as sess:
            try:
                load(sess, filepath=self.model_save_path + 'weights.mdl')
            except errors.NotFoundError as e:
                logging.info('No models found. Aborting...\n' + str(e))
                # print(str(e))
                raise IOError('No models found. Aborting...\n' + str(e))
            except errors.InvalidArgumentError as e:
                logging.info('Model of different architecture found. Aborting...\n' + str(e))
                # print(str(e))
                raise IOError('Model of different architecture found. Aborting...\n' + str(e))
            for img_batch_raw, filename in gen.yield_predict_batch_ar(1):
                img_batch = img_batch_raw * 2 - 1
                import time
                a = time.time()
                img = sess.run(fetches=(self.fake_image), feed_dict={self.input_image: img_batch})
                print (time.time() - a)
                pred_img = img[0, :, :, 0]
                pred_img = (pred_img + 1) / 2.0
                save_image((pred_img * 255).astype(np.uint8), self.img_save_path + filename.replace('.jpg', ''))

    def visualize_filters(self, ip, img=0):
        filters = int(ip.shape[-1])
        row, col = self.factors(filters)
        plt.subplots(nrows=row, ncols=col)
        for i in range(filters):
            plt.subplot(row, col, i + 1)
            plt.imshow(ip[img, :, :, i], cmap='hot', interpolation='nearest')
        plt.show()

    @staticmethod
    def factors(n):
        if is_square(n):
            return (int(np.sqrt(n)), int(np.sqrt(n)))
        fac = set(reduce(list.__add__, ([i, n // i] for i in range(1, int(pow(n, 0.5) + 1)) if n % i == 0)))
        lim = np.sqrt(n)
        fac_1 = []
        fac_2 = []
        for f in fac:
            if f < lim:
                fac_1.append(f)
            else:
                fac_2.append(f)
        return (max(fac_1), min(fac_2))


if __name__ == '__main__':
    # gt_path_ = parser.get('DataGeneratorIMNETAR', 'gt')
    # tr_path_ = parser.get('DataGeneratorIMNETAR', 'tr')
    # test_path = parser.get('DataGeneratorIMNETAR', 'test_data_path')
    # test_path_gt = parser.get('DataGeneratorIMNETAR', 'test_data_path_gt')
    # num_gpu = int(parser.get('main', 'num_GPU'))
    # GPU = parser.get('main', 'GPU').split()
    # assert num_gpu == len(GPU)
    # batch_size_ = int(parser.get('main', 'batch_size'))
    # check_every_ = int(parser.get('main', 'check_every'))
    # gen_img_save_path = parser.get('main', 'save_path')
    # f = ArtifactRemoval(batch_size_, 0.001, 100, 200, check_every=check_every_, channels=1)
    # # f.fit_graph(tr_path_, gt_path_)
    # f.predict(test_path)
    pass
