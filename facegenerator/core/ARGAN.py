from __future__ import print_function
import errno
import os
import sys
from os.path import abspath
from os.path import dirname
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from PIL import Image
from tensorflow import errors
from facegenerator.utils import DataGenerator
from facegenerator.utils.visualize import plot_data, save_image
from facegenerator.utils.visualize import plot_generated
from facegenerator.utils.metrics import exp_loss
from facegenerator.utils.metrics import l1_loss
from facegenerator.utils.metrics import psnr
from facegenerator.utils.metrics import ssim, gmsd
from facegenerator.core.etc import save
from facegenerator.core.etc import load
from models import u_net_ar_v1, dis_autoencoder_v1
from facegenerator.utils.metrics import haarpsi
from models import Params
from functools import reduce

# try:
#     import gmpy2 as gmp
# except ImportError:
#     import gmpy as gmp
import logging
import logging.config
from time import time
import matplotlib.pyplot as plt
from tensorflow import nn

if sys.version_info.major == 2:
    from ConfigParser import SafeConfigParser

    parser = SafeConfigParser()
else:
    from configparser import ConfigParser

    parser = ConfigParser()

my_path = os.path.abspath(os.path.dirname(__file__))
log_file_path = os.path.join(my_path, '../../logging.ini')
config_file_path = os.path.join(my_path, '../../config.ini')
parser.read(config_file_path)
logging.config.fileConfig(log_file_path)
gen_img_save_path = parser.get('main', 'save_path')
model_path = parser.get('main', 'model_path')


def rgb2gray(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray


class SRandOcclusion(object):
    def __init__(self, batch_size, learning_rate, epoch, num_classes, **kwargs):
        allowed_kwargs = {'check_every',
                          'channels'}
        for k in kwargs:
            if k not in allowed_kwargs:
                raise TypeError('Unexpected keyword argument passed: ' + str(k))
        self.__dict__.update(kwargs)
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.epoch = epoch
        self.num_classes = num_classes
        self.training_loss = {'gmsd': [], 'haarpsi': []}
        self.k_t = 0
        self.kappa = 1.0
        self.alpha = 0.5
        self.optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)

    def generator(self, x_in, reuse=False):
        """
        The generator of the GAN
        :param x_in:
        :param reuse:
        :return: The output of the generator.
        """
        kwargs = {'base_filters': 16,
                  'kernel_size': (7, 7),
                  'strides': (2, 2),
                  'dim_encoding': 64,
                  'num_classes': self.num_classes,
                  'channels': self.channels,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('generator', reuse=reuse):
            logging.info('U-Net generator in action')
            x = u_net_ar_v1(x_in, params=u_net_params, reuse=reuse)
            return x

    def discriminator(self, x_in, reuse=None):
        kwargs = {'base_filters': 16,
                  'kernel_size': (3, 3),
                  'strides': (1, 1),
                  'num_classes': self.num_classes,
                  'channels': self.channels,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('discriminator', reuse=reuse):
            x, x_out = dis_autoencoder_v1(x_in, params=u_net_params, reuse=reuse, algo='ar')
            return x, x_out

    def fit_graph(self, tr_path, gt_path, ignore_list_path=None):
        gamma = self.gamma if hasattr(self, 'gamma') else 0.7
        self.channels = self.channels if hasattr(self, 'channels') else 3
        self.training_image = tf.placeholder(dtype=tf.float32, shape=(None, None, None, self.channels),
                                             name='gen_input')
        self.ground_truth = tf.placeholder(dtype=tf.float32, shape=(None, None, None, self.channels),
                                           name='ground_truth')
        self.fake_image = self.generator(self.training_image)
        d_real_logit, discriminator_output_real = self.discriminator(self.ground_truth)
        d_fake_logit, discriminator_output_fake = self.discriminator(self.fake_image, reuse=True)
        loss_real = exp_loss(discriminator_output_real, self.ground_truth)
        loss_fake = exp_loss(discriminator_output_fake, self.fake_image)
        self.d_loss = exp_loss(loss_real, self.k_t * loss_fake)
        self.g_loss = exp_loss(self.fake_image, self.ground_truth)
        trainable_vars = tf.trainable_variables()
        dcr_vars = [var for var in trainable_vars if 'discriminator' in var.name]
        gen_vars = [var for var in trainable_vars if 'generator' in var.name]
        self.opt_g = self.optimizer.minimize(loss=self.g_loss, var_list=gen_vars)
        self.opt_d = self.optimizer.minimize(loss=self.d_loss, var_list=dcr_vars + gen_vars)
        self.k_t = tf.reduce_min([tf.reduce_max([self.k_t + np.abs(gamma * loss_real - loss_fake), 0.]), 1.])
        kwargs = {'data_resolution': (256, 256),
                  'gt_resolution': (256, 256)}
        gen = DataGenerator(tr_path, gt_path, **kwargs)
        self.train(gen)

    def train(self, data_generator):
        global save_path
        check_every = self.check_every if hasattr(self, 'check_every') else 25
        counter = 0
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            try:
                load(sess, filepath=model_path + 'model/weights.mdl')
            except errors.NotFoundError:
                logging.info('No models found, training from start')
            except errors.InvalidArgumentError:
                logging.info('Model of different architecture found. Need to clean it up before proceeding')
                raise IOError('Model of different architecture found. Remove it before proceeding')
            try:
                for epoch in tqdm(range(self.epoch)):
                    # for training_batch, gt_batch in data_generator.yield_arsr_batch(self.batch_size):
                    for training_batch, gt_batch in data_generator.yield_ar_batch_v1(self.batch_size):
                        # convert the data for tanh range
                        gt_batch = gt_batch * 2 - 1
                        training_batch = training_batch * 2 - 1
                        counter += 1
                        if np.mod(counter, check_every) != 0:

                            _, gen_loss = sess.run(fetches=[self.opt_g, self.g_loss],
                                                   feed_dict={self.training_image: training_batch,
                                                              self.ground_truth: gt_batch})
                            _, dis_loss, k_t = sess.run(
                                fetches=[self.opt_d, self.d_loss, self.k_t],
                                feed_dict={self.training_image: training_batch,
                                           self.ground_truth: gt_batch})
                        else:
                            # plot the data
                            img = sess.run(fetches=(self.fake_image), feed_dict={self.training_image: training_batch})
                            img = (img + 1) / 2.0
                            gt = (gt_batch + 1) / 2.0
                            # tr = (training_batch + 1) / 2.0
                            psnr_ = psnr(gt.astype(np.float32), img)
                            ssim_ = ssim(gt.astype(np.float32), img, multichannel=True)
                            gmsd_ = gmsd(gt, img)
                            haarpsi_ = haarpsi(gt, img, True)
                            # ----------------------------------------------------------------------
                            # plot the training loss
                            # self.training_loss['gmsd'].append(np.float16(gmsd_))
                            # self.training_loss['haarpsi'].append(np.float16(haarpsi_))
                            # -----------------------------------------------------------------------
                            # plot images from the test set
                            gen = DataGenerator(test_path)
                            for img_batch_raw, filename in gen.yield_predict_batch_ar(1):
                                img_batch = img_batch_raw * 2 - 1
                                img = sess.run(fetches=(self.fake_image), feed_dict={self.training_image: img_batch})
                                pred_img = img[0, :, :, 0]
                                pred_img = (pred_img + 1) / 2.0
                                save_image(pred_img, gen_img_save_path + filename.replace('.jpg', ''))
                            # -----------------------------------------------------------------------
                            print('PSNR: %f, SSIM: %f, GMSD: %f, HaarPSI: %f' % (psnr_, ssim_, gmsd_, haarpsi_))
                            # ==========================================================================
                            # plot images from training set
                            # save_path = gen_img_save_path
                            # plot_generated(img, save=True, img_path=save_path + 'op/', show_img=False)
                            # plot_generated(gt, save=True, img_path=save_path + 'gt/', show_img=False)
                            # plot_generated(tr, save=True, img_path=save_path + 'tr/', show_img=False)
                            # ==========================================================================
                            logging.debug('%d epochs completed' % epoch)
                            save(sess, model_path + '/model/weights.mdl')
                            logging.info('Saved model successfully')
                            counter = 0
            except KeyboardInterrupt:
                logging.info('Early break of loop due to keyboard interruption.')
                save(sess, gen_img_save_path)
                logging.info('Saved model successfully')
        logging.info('Training completed.')

    def predict(self, cond_img_path, gt_path=None):
        """
        Predict the output of a full image of any size.
        :param cond_img_path: 
        :return: 
        """
        gen = DataGenerator(cond_img_path)
        self.input_image = tf.placeholder(dtype=tf.float32, shape=(None, None, None, 1),
                                          name='gen_input')
        self.fake_image = self.generator(self.input_image)
        with tf.Session() as sess:
            try:
                load(sess, filepath=model_path + 'model/weights.mdl')
            except:
                print('No model found')
            for img_batch_raw, filename in gen.yield_predict_batch_ar(1):
                img_batch = img_batch_raw * 2 - 1
                img = sess.run(fetches=(self.fake_image), feed_dict={self.input_image: img_batch})
                pred_img = img[0, :, :, 0]
                pred_img = (pred_img + 1) / 2.0
                save_image(pred_img * 255, gen_img_save_path + filename.replace('.jpg', ''))
    #
    # def visualize_filters(self, ip, img=0):
    #     filters = int(ip.shape[-1])
    #     row, col = self.factors(filters)
    #     plt.subplots(nrows=row, ncols=col)
    #     for i in range(filters):
    #         plt.subplot(row, col, i + 1)
    #         plt.imshow(ip[img, :, :, i], cmap='hot', interpolation='nearest')
    #     plt.show()
    #
    # @staticmethod
    # def factors(n):
    #     if gmp.is_square(n):
    #         return (int(np.sqrt(n)), int(np.sqrt(n)))
    #     fac = set(reduce(list.__add__, ([i, n // i] for i in range(1, int(pow(n, 0.5) + 1)) if n % i == 0)))
    #     lim = np.sqrt(n)
    #     fac_1 = []
    #     fac_2 = []
    #     for f in fac:
    #         if f < lim:
    #             fac_1.append(f)
    #         else:
    #             fac_2.append(f)
    #     return (max(fac_1), min(fac_2))

    def best_psnr(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _psnr = psnr(gt[i], img[i])
            if _psnr > self.best_psnr_:
                self.best_psnr_ = _psnr
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/psnr_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/psnr_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/psnr_ip', show_img=False)
                best.append(_psnr)
        try:
            print(max(best))
        except ValueError:
            pass

    def best_gmsd(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _gmsd = gmsd(gt[i], img[i])
            if _gmsd < self.best_gmsd_:
                self.best_gmsd_ = _gmsd
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/gmsd_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/gmsd_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/gmsd_ip', show_img=False)
                best.append(_gmsd)
        try:
            print('best gmsd ' + str(max(best)))
        except ValueError as err:
            pass

    def best_ssim(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _ssim = ssim(gt[i], img[i], multichannel=True)
            if _ssim > self.best_ssim_:
                self.best_ssim_ = _ssim
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/ssim_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/ssim_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/ssim_ip', show_img=False)
                best.append(_ssim)
        try:
            print(max(best))
        except ValueError as err:
            pass

    def worst_psnr(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _psnr = psnr(gt[i], img[i])
            if _psnr < self.worst_psnr_:
                self.worst_psnr_ = _psnr
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worstpsnr_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worstpsnr_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worstpsnr_ip', show_img=False)
                worst.append(_psnr)
        try:
            print(min(worst))
        except ValueError as err:
            pass

    def worst_gmsd(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _gmsd = gmsd(gt[i], img[i])
            if _gmsd > self.worst_gmsd_:
                self.worst_gmsd_ = _gmsd
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worstgmsd_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worstgmsd_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worstgmsd_ip', show_img=False)
                worst.append(_gmsd)
        try:
            print('worst gmsd ' + str(min(worst)))
        except ValueError as err:
            pass

    def worst_ssim(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _ssim = ssim(gt[i], img[i], multichannel=True)
            if _ssim < self.worst_ssim_:
                self.worst_ssim_ = _ssim
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worstssim_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worstssim_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worstssim_ip', show_img=False)
                worst.append(_ssim)
        try:
            print(min(worst))
        except ValueError as err:
            pass

    def best_haarpsi(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _haarpsi = haarpsi(rgb2gray(gt[i]), rgb2gray(img[i]))[0]
            if _haarpsi > self.best_haarpsi_:
                self.best_haarpsi_ = _haarpsi
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/besthaarpsi_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/besthaarpsi_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/besthaarpsi_ip', show_img=False)
                best.append(_haarpsi)
        try:
            print('best haarpsi ' + str(max(best)))
        except ValueError as err:
            pass

    def worst_haarpsi(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _haarpsi = haarpsi(rgb2gray(gt[i]), rgb2gray(img[i]))[0]
            if _haarpsi < self.worst_haarpsi_:
                self.worst_haarpsi_ = _haarpsi
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worsthaarpsi_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worsthaarpsi_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worsthaarpsi_ip', show_img=False)
                worst.append(_haarpsi)
        try:
            print('worst haarpsi ' + str(min(worst)))
        except ValueError as err:
            pass


if __name__ == '__main__':
    gt_path_ = parser.get('DataGeneratorIMNETAR', 'gt')
    tr_path_ = parser.get('DataGeneratorIMNETAR', 'tr')
    test_path = parser.get('DataGeneratorIMNETAR', 'test_data_path')
    test_path_gt = parser.get('DataGeneratorIMNETAR', 'test_data_path_gt')
    batch_size_ = int(parser.get('main', 'batch_size'))
    check_every_ = int(parser.get('main', 'check_every'))
    gen_img_save_path = parser.get('main', 'save_path')
    f = SRandOcclusion(batch_size_, 0.001, 100, 200, check_every=check_every_, channels=1)
    f.fit_graph(tr_path_, gt_path_)
    # f.predict(test_path)
