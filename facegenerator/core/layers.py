import tensorflow as tf
import sys
import os
import logging.config
import logging

from tensorflow import depth_to_space

if sys.version_info.major == 2:
    from ConfigParser import SafeConfigParser

    parser = SafeConfigParser()
else:
    from configparser import ConfigParser

    parser = ConfigParser()

my_path = os.path.abspath(os.path.dirname(__file__))
log_file_path = os.path.join(my_path, '../../logging.ini')
config_file_path = os.path.join(my_path, '../../config.ini')
parser.read(config_file_path)
logging.config.fileConfig(log_file_path)

bias = False


def conv2d(input_, filters, kernel_size, strides, padding, bias=bias, name=None):
    ip_channel = input_.get_shape().as_list()[-1]
    dims = [kernel_size[0], kernel_size[1], ip_channel, filters]
    filter = tf.get_variable('filter', dims, dtype=tf.float32, initializer=tf.random_normal_initializer(0, 0.02))
    strides = [1, strides[0], strides[1], 1]
    conv = tf.nn.conv2d(input_, filter, strides=strides, padding=padding, name=name)
    if bias:
        b_init = tf.random_normal_initializer(0, 0.002)
        b = tf.get_variable('b', shape=(filters,), initializer=b_init)
        conv = tf.nn.bias_add(conv, b)
    return conv


def conv2d_transpose(input_, filters, kernel_size, strides, padding, bias=bias, name=None):
    ip_shape = input_.get_shape().as_list()
    ip_channel = ip_shape[-1]
    dims = [kernel_size[0], kernel_size[1], filters, ip_channel]  # note the difference with 'dims' of conv2d
    filter_ = tf.get_variable('filter', dims, dtype=tf.float32, initializer=tf.random_normal_initializer(0, 0.02))
    strides = [1, strides[0], strides[1], 1]
    output_shape = [ip_shape[0], tf.multiply(ip_shape[1], 2), tf.multiply(ip_shape[2], 2), filters]
    conv = tf.nn.conv2d_transpose(input_, filter_, output_shape, strides, padding, name=name)
    if bias:
        b_init = tf.random_normal_initializer(0, 0.002)
        b = tf.get_variable('b', shape=(filters,), initializer=b_init)
        conv = tf.nn.bias_add(conv, b)
    return conv


def leakyrelu(input_, alpha=0.2, name=None):
    # adding these together creates the leak part and linear part
    # then cancels them out by subtracting/adding an absolute value term
    # leak: alpha*input/2 - alpha*abs(input)/2
    # linear: input/2 + abs(input)/2
    # this block looks like it has 2 inputs on the graph unless we do this
    # taken from https://github.com/affinelayer/pix2pix-tensorflow/blob/master/pix2pix.py
    name = name or 'lrelu'
    input_ = tf.identity(input_)
    with tf.name_scope(name):
        op = (0.5 * (1 + alpha)) * input_ + (0.5 * (1 - alpha)) * tf.abs(input_)
    return op


def prelu(_x):
    alphas = tf.get_variable('alpha', _x.get_shape()[-1],
                             initializer=tf.constant_initializer(0.0),
                             dtype=tf.float32)
    pos = tf.nn.relu(_x)
    neg = alphas * (_x - abs(_x)) * 0.5

    return pos + neg


def batch_normalization(input_, name=None):
    # this block looks like it has 3 inputs on the graph unless we do this
    input_ = tf.identity(input_)
    channels = input_.get_shape().as_list()[-1]
    offset = tf.get_variable('bn_offset', [channels], dtype=tf.float32, initializer=tf.zeros_initializer())
    scale = tf.get_variable('bn_scale', [channels], dtype=tf.float32,
                            initializer=tf.random_normal_initializer(1.0, 0.02))
    mean, variance = tf.nn.moments(input_, axes=[0, 1, 2], keep_dims=False)
    variance_epsilon = 1e-5
    return tf.nn.batch_normalization(input_, mean, variance, offset, scale, variance_epsilon=variance_epsilon,
                                     name=name)


def batch_normalization_adv(input_, is_training, decay=0.99, epsilon=0.001, trainable=True):
    """
    A more sophisticated Batch Normalization from the paper Batch Normalization: Accelerating Deep Network Training by
    Reducing Internal Covariate Shift
    https://arxiv.org/pdf/1502.03167.pdf
    :param input_: 
    :param is_training: True during training, False during testing.
    :param decay: 
    :param epsilon: 
    :param trainable: 
    :return: 
    """

    def bn_train():
        batch_mean, batch_var = tf.nn.moments(input_, axes=[0, 1, 2])
        train_mean = tf.assign(pop_mean, pop_mean * decay + batch_mean * (1 - decay))
        train_var = tf.assign(pop_var, pop_var * decay + batch_var * (1 - decay))
        with tf.control_dependencies([train_mean, train_var]):
            return tf.nn.batch_normalization(input_, batch_mean, batch_var, beta, scale, epsilon)

    def bn_inference():
        return tf.nn.batch_normalization(input_, pop_mean, pop_var, beta, scale, epsilon)

    dim = input_.get_shape().as_list()[-1]
    beta = tf.get_variable(name='beta',
                           shape=[dim],
                           dtype=tf.float32,
                           initializer=tf.truncated_normal_initializer(stddev=0.0),
                           trainable=trainable)
    scale = tf.get_variable(name='scale',
                            shape=[dim],
                            dtype=tf.float32,
                            initializer=tf.truncated_normal_initializer(stddev=0.1),
                            trainable=trainable)
    pop_mean = tf.get_variable(name='pop_mean',
                               shape=[dim],
                               dtype=tf.float32,
                               initializer=tf.constant_initializer(0.0),
                               trainable=False)
    pop_var = tf.get_variable(name='pop_var',
                              shape=[dim],
                              dtype=tf.float32,
                              initializer=tf.constant_initializer(1.0),
                              trainable=False)
    if is_training:
        return bn_train()
    else:
        return bn_inference()


def max_pooling(input_, kernel_size, strides, padding, name=None):
    kernel = [1, kernel_size[0], kernel_size[1], 1]
    stride = [1, strides[0], strides[1], 1]
    return tf.nn.max_pool(input_, kernel, stride, padding=padding, name=name)


def avg_pooling(input_, kernel_size, strides, padding, name=None):
    kernel = [1, kernel_size[0], kernel_size[1], 1]
    stride = [1, strides[0], strides[1], 1]
    return tf.nn.avg_pool(input_, kernel, stride, padding=padding, name=name)


def maxout_unit(input_, filters, kernel_size, strides, padding, bias=True, name=None):
    # one unit of the maxout network
    name = name or 'maxout_unit'
    with tf.variable_scope(name):
        conv = conv2d(input_, filters, kernel_size, strides, padding, bias=bias)
        slice_1, slice_2 = conv[:, :, :, :filters / 2], conv[:, :, :, filters / 2:]
        return tf.maximum(slice_1, slice_2, name=name)


# def prelu(input_, name=None):
#     alphas = tf.get_variable('alpha', input_.get_shape()[-1], initializer=tf.constant_initializer(0.0))
#     with tf.name_scope(name):
#         pos = tf.nn.relu(input_)
#         neg = alphas * (input_ - abs(input_)) * 0.5
#     return pos + neg


def pixelshuffle(input_, r):
    """
    Main OP that you can arbitrarily use in you tensorflow code
    :param input_: 
    :param r: upscale factor
    :return: 
    """

    return tf.depth_to_space(input_, r)
    # def ps(x, r):
    #     bs, a, b, c = x.get_shape().as_list()
    #     x = tf.reshape(x, (bs, a, b, r, r))
    #     x = tf.transpose(x, (0, 1, 2, 4, 3))
    #     x = tf.split(x, a, 1)
    #     x = tf.concat([tf.squeeze(x_) for x_ in x], 2)
    #     x = tf.split(x, b, 1)
    #     x = tf.concat([tf.squeeze(x_) for x_ in x], 2)
    #     return tf.reshape(x, (bs, a * r, b * r, 1))
    #
    # xc = tf.split(input_, n_split, 3)
    # return tf.concat([ps(x_, r) for x_ in xc], 3)

# def _PS(X, r, n_out_channels):
#     if n_out_channels >= 1:
#         if int(X.get_shape()[-1]) != (r ** 2) * n_out_channels:
#             raise Exception(_err_log)
#         # bsize, a, b, c = X.get_shape().as_list()
#         # bsize = tf.shape(X)[0] # Handling Dimension(None) type for undefined batch dim
#         # Xs=tf.split(X,r,3) #b*h*w*r*r
#         # Xr=tf.concat(Xs,2) #b*h*(r*w)*r
#         # X=tf.reshape(Xr,(bsize,r*a,r*b,n_out_channel)) # b*(r*h)*(r*w)*c
#
#         X = tf.depth_to_space(X, r)
#     else:
#         logging.info(_err_log)
#     return X
#
#
# if n_out_channel is None:
#     if int(self.inputs.get_shape()[-1]) / (scale ** 2) % 1 != 0:
#         raise Exception(_err_log)
#     n_out_channel = int(int(self.inputs.get_shape()[-1]) / (scale ** 2))
#
# with tf.variable_scope(name):
#     self.outputs = act(_PS(self.inputs, r=scale, n_out_channels=n_out_channel))
#
# self.all_layers.append(self.outputs)
