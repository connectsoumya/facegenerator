from __future__ import print_function
import errno
import os
import sys
from os.path import abspath
from os.path import dirname
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from facegenerator.utils import DataGenerator
from facegenerator.utils.visualize import plot_data
from facegenerator.utils.visualize import plot_generated
from facegenerator.utils.metrics import exp_loss
from facegenerator.utils.metrics import l1_loss
from facegenerator.utils.metrics import psnr
from facegenerator.utils.metrics import ssim, gmsd
from facegenerator.core.etc import save
from facegenerator.core.etc import load
from models import dis_decoder
from models import dis_encoder
from models import u_net_arsr_4x, scl_generator
from models import u_net_arsr_4x_v2
from facegenerator.utils._haarpsi import haar_psi
from models import Params
from functools import reduce
import gmpy
import logging
import logging.config
from time import time
import matplotlib.pyplot as plt
from tensorflow import nn


if sys.version_info.major == 2:
    from ConfigParser import SafeConfigParser

    parser = SafeConfigParser()
else:
    from configparser import ConfigParser

    parser = ConfigParser()

d = os.path.expanduser('~') + '/facegenerator/settings' # Change this with absolute path if in a different location
parser.read(d + '/config.ini')
logging.config.fileConfig(d + '/logging.ini')
gen_img_save_path = parser.get('main', 'save_path')


def rgb2gray(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray


class SRandOcclusion(object):
    def __init__(self, batch_size, learning_rate, epoch, num_classes, **kwargs):
        allowed_kwargs = {'check_every'}
        for k in kwargs:
            if k not in allowed_kwargs:
                raise TypeError('Unexpected keyword argument passed: ' + str(k))
        self.__dict__.update(kwargs)
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.epoch = epoch
        self.num_classes = num_classes
        self.training_loss = {'gmsd': [], 'haarpsi': []}
        self.k_t = 0
        self.kappa = 1.0
        self.alpha = 0.5
        self.optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)

    def generator(self, x_in, reuse=False):
        """
        The generator of the GAN
        :param x_in:
        :param reuse:
        :return: The output of the generator.
        """
        kwargs = {'base_filters': 8,
                  'kernel_size': (4, 4),
                  'strides': (2, 2),
                  'dim_encoding': 64,
                  'num_classes': self.num_classes,
                  'channels': 3,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('generator', reuse=reuse):
            logging.info('U-Net generator in action')
            x = scl_generator(x_in, params=u_net_params, reuse=reuse)
            return x

    def discriminator(self, x_in, reuse=None):
        kwargs = {'base_filters': 8,
                  'kernel_size': (3, 3),
                  'strides': (1, 1),
                  'dim_encoding': 64,
                  'num_classes': self.num_classes,
                  'channels': 3,
                  'batch_size': self.batch_size, }
        u_net_params = Params(**kwargs)
        with tf.variable_scope('discriminator', reuse=reuse):
            x_enc = dis_encoder(x_in, params=u_net_params, reuse=reuse)
            x, x_out = dis_decoder(x_enc, params=u_net_params, reuse=reuse)
            return x, x_out

    # trial 2

    def fit_graph(self, tr_path, gt_path, ignore_list_path=None):
        gamma = self.gamma if hasattr(self, 'gamma') else 0.7
        image_shape = self.resize_to + (3,) if hasattr(self, 'resize_to') else (256, 256, 3)
        self.channels = self.channels if hasattr(self, 'channels') else 3
        batch_shape = (self.batch_size,) + image_shape
        self.training_image = tf.placeholder(dtype=tf.float32, shape=(self.batch_size, 64, 64, 3), name='gen_input')
        self.ground_truth = tf.placeholder(dtype=tf.float32, shape=batch_shape, name='ground_truth')
        # self.label = tf.placeholder(dtype=tf.int32, shape=(None,), name='label')
        self.fake_image = self.generator(self.training_image)
        d_real_logit, discriminator_output_real = self.discriminator(self.ground_truth)
        d_fake_logit, discriminator_output_fake = self.discriminator(self.fake_image, reuse=True)
        loss_real = exp_loss(discriminator_output_real, self.ground_truth)
        loss_fake = exp_loss(discriminator_output_fake, self.fake_image)
        self.d_loss = exp_loss(loss_real, self.k_t * loss_fake)
        self.g_loss = exp_loss(self.fake_image, self.ground_truth)
        trainable_vars = tf.trainable_variables()
        dcr_vars = [var for var in trainable_vars if 'discriminator' in var.name]
        gen_vars = [var for var in trainable_vars if 'generator' in var.name]
        self.opt_g = self.optimizer.minimize(loss=self.g_loss, var_list=gen_vars)
        self.opt_d = self.optimizer.minimize(loss=self.d_loss, var_list=dcr_vars + gen_vars)
        self.k_t = tf.reduce_min([tf.reduce_max([self.k_t + np.abs(gamma * loss_real - loss_fake), 0.]), 1.])
        kwargs = {'data_resolution': (64, 64),
                  'gt_resolution': (256, 256)}
        gen = DataGenerator(tr_path, gt_path, **kwargs)
        self.train(gen)

    def train(self, data_generator):
        global save_path
        check_every = self.check_every if hasattr(self, 'check_every') else 25
        counter = 0
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            try:
                for epoch in tqdm(range(self.epoch)):
                    # for training_batch, gt_batch in data_generator.yield_arsr_batch(self.batch_size):
                    for training_batch, gt_batch in data_generator.yield_arsr_with_occlusion(self.batch_size):
                        # convert the data for tanh range
                        gt_batch = gt_batch * 2 - 1
                        training_batch = training_batch * 2 - 1
                        counter += 1
                        if np.mod(counter, check_every) != 0:

                            _, gen_loss = sess.run(fetches=[self.opt_g, self.g_loss],
                                                   feed_dict={self.training_image: training_batch,
                                                              self.ground_truth: gt_batch})
                            _, dis_loss, k_t = sess.run(
                                fetches=[self.opt_d, self.d_loss, self.k_t],
                                feed_dict={self.training_image: training_batch,
                                           self.ground_truth: gt_batch})
                        else:
                            # plot the data
                            img = sess.run(fetches=(self.fake_image), feed_dict={self.training_image: training_batch})
                            img = (img + 1) / 2.0
                            gt = (gt_batch + 1) / 2.0
                            tr = (training_batch + 1) / 2.0
                            gmsd_ = gmsd(gt, img)
                            haarpsi_ = haar_psi(rgb2gray(gt), rgb2gray(img), False)[0]
                            self.training_loss['gmsd'].append(np.float16(gmsd_))
                            self.training_loss['haarpsi'].append(np.float16(haarpsi_))
                            save_path = gen_img_save_path
                            plot_generated(img, save=True, img_path=save_path + 'op/', show_img=False)
                            plot_generated(gt, save=True, img_path=save_path + 'gt/', show_img=False)
                            plot_generated(tr, save=True, img_path=save_path + 'tr/', show_img=False)
                            plot_data(data=self.training_loss, img_path=save_path)
                            logging.debug('%d epochs completed' % epoch)
                            save(sess, save_path + '/model/weights.mdl')
                            logging.info('Saved model successfully')
                            counter = 0
            except KeyboardInterrupt:
                logging.info('Early break of loop due to keyboard interruption.')
                save(sess, save_path)
                logging.info('Saved model successfully')
        logging.info('Training completed.')

    def predict(self, cond_img_path, gt_path=None):
        """

        :param cond_img_path: 
        :return: 
        """
        data_res = (64, 64)
        kwargs = {'data_resolution': data_res,
                  'gt_resolution': (256, 256)}
        gen = DataGenerator(cond_img_path, gt_path, **kwargs)
        self.input_image = tf.placeholder(dtype=tf.float32, shape=(self.batch_size,) + data_res + (3,),
                                          name='gen_input')
        _, self.fake_image, self.intr = self.generator(self.input_image)
        with tf.Session() as sess:
            load(sess, filepath=gen_img_save_path + '/model/weights.mdl')
            gmsd_ = []
            haarpsi_ = []
            t = []
            self.best_psnr_ = 0
            self.worst_psnr_ = np.inf
            self.best_ssim_ = 0
            self.worst_ssim_ = np.inf
            self.best_gmsd_ = np.inf
            self.worst_gmsd_ = 0
            self.best_haarpsi_ = 0
            self.worst_haarpsi_ = np.inf
            for img_batch_raw, gt in gen.yield_arsr_with_occlusion(self.batch_size):
                img_batch = img_batch_raw * 2 - 1
                t1 = time()
                img, fils= sess.run(fetches=(self.fake_image, self.intr), feed_dict={self.input_image: img_batch})

                # self.visualize_filters(fils)

                t2 = time() - t1
                img = (img + 1) / 2.0
                t.append(t2)
                gmsd_.append(gmsd(img, gt))
                haarpsi_.append(haar_psi(rgb2gray(gt), rgb2gray(img))[0])
                self.best_gmsd(np.float32(gt), img, img_batch_raw)
                self.worst_gmsd(np.float32(gt), img, img_batch_raw)
                self.best_haarpsi(np.float32(gt), img, img_batch_raw)
                self.worst_haarpsi(np.float32(gt), img, img_batch_raw)
                print('-')
            print('-------------------------------------')
            print(np.mean(gmsd_))
            print(np.mean(haarpsi_))
            print(np.mean(t))

    def visualize_filters(self, ip, img=0):
        filters = int(ip.shape[-1])
        row, col = self.factors(filters)
        plt.subplots(nrows=row, ncols=col)
        for i in range(filters):
            plt.subplot(row, col, i + 1)
            plt.imshow(ip[img, :, :, i], cmap='hot', interpolation='nearest')
        plt.show()

    @staticmethod
    def factors(n):
        if gmpy.is_square(n):
            return (int(np.sqrt(n)), int(np.sqrt(n)))
        fac = set(reduce(list.__add__, ([i, n // i] for i in range(1, int(pow(n, 0.5) + 1)) if n % i == 0)))
        lim = np.sqrt(n)
        fac_1 = []
        fac_2 = []
        for f in fac:
            if f < lim:
                fac_1.append(f)
            else:
                fac_2.append(f)
        return (max(fac_1), min(fac_2))

    def best_psnr(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _psnr = psnr(gt[i], img[i])
            if _psnr > self.best_psnr_:
                self.best_psnr_ = _psnr
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/psnr_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/psnr_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/psnr_ip', show_img=False)
                best.append(_psnr)
        try:
            print(max(best))
        except ValueError:
            pass

    def best_gmsd(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _gmsd = gmsd(gt[i], img[i])
            if _gmsd < self.best_gmsd_:
                self.best_gmsd_ = _gmsd
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/gmsd_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/gmsd_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/gmsd_ip', show_img=False)
                best.append(_gmsd)
        try:
            print('best gmsd ' + str(max(best)))
        except ValueError as err:
            pass

    def best_ssim(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _ssim = ssim(gt[i], img[i])
            if _ssim > self.best_ssim_:
                self.best_ssim_ = _ssim
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/ssim_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/ssim_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/ssim_ip', show_img=False)
                best.append(_ssim)
        try:
            print(max(best))
        except ValueError as err:
            pass

    def worst_psnr(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _psnr = psnr(gt[i], img[i])
            if _psnr < self.worst_psnr_:
                self.worst_psnr_ = _psnr
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worstpsnr_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worstpsnr_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worstpsnr_ip', show_img=False)
                worst.append(_psnr)
        try:
            print(min(worst))
        except ValueError as err:
            pass

    def worst_gmsd(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _gmsd = gmsd(gt[i], img[i])
            if _gmsd > self.worst_gmsd_:
                self.worst_gmsd_ = _gmsd
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worstgmsd_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worstgmsd_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worstgmsd_ip', show_img=False)
                worst.append(_gmsd)
        try:
            print('worst gmsd ' + str(min(worst)))
        except ValueError as err:
            pass

    def worst_ssim(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _ssim = ssim(gt[i], img[i])
            if _ssim < self.worst_ssim_:
                self.worst_ssim_ = _ssim
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worstssim_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worstssim_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worstssim_ip', show_img=False)
                worst.append(_ssim)
        try:
            print(min(worst))
        except ValueError as err:
            pass

    def best_haarpsi(self, gt, img, ip):
        best = []
        for i in range(gt.shape[0]):
            _haarpsi = haar_psi(rgb2gray(gt[i]), rgb2gray(img[i]))[0]
            if _haarpsi > self.best_haarpsi_:
                self.best_haarpsi_ = _haarpsi
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/besthaarpsi_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/besthaarpsi_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/besthaarpsi_ip', show_img=False)
                best.append(_haarpsi)
        try:
            print('best haarpsi ' + str(max(best)))
        except ValueError as err:
            pass

    def worst_haarpsi(self, gt, img, ip):
        worst = []
        for i in range(gt.shape[0]):
            _haarpsi = haar_psi(rgb2gray(gt[i]), rgb2gray(img[i]))[0]
            if _haarpsi < self.worst_haarpsi_:
                self.worst_haarpsi_ = _haarpsi
                plot_generated(img[i], save=True, img_path=gen_img_save_path + '/worsthaarpsi_op', show_img=False)
                plot_generated(gt[i], save=True, img_path=gen_img_save_path + '/worsthaarpsi_gt', show_img=False)
                plot_generated(ip[i], save=True, img_path=gen_img_save_path + '/worsthaarpsi_ip', show_img=False)
                worst.append(_haarpsi)
        try:
            print('worst haarpsi ' + str(min(worst)))
        except ValueError as err:
            pass


if __name__ == '__main__':
    gt_path_ = parser.get('DataGeneratorARSR', 'face_gt')
    tr_path_ = parser.get('DataGeneratorARSR', 'face_tr')
    test_path = parser.get('DataGeneratorARSR', 'test_data_path')
    test_path_gt = parser.get('DataGeneratorARSR', 'test_data_path_gt')
    # ignore_list_path_ = parser.get('DataGenerator', 'ignore')
    batch_size_ = int(parser.get('main', 'batch_size'))
    check_every_ = int(parser.get('main', 'check_every'))
    gen_img_save_path = parser.get('main', 'save_path')
    f = SRandOcclusion(batch_size_, 0.001, 100, 200, check_every=check_every_)
    f.fit_graph(tr_path_, gt_path_)
    # f.predict(test_path, test_path_gt)
