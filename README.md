# Author: Soumya Shubhra Ghosh

### logging.ini

```ini
[loggers]
keys=root

[handlers]
keys=consoleHandler, fileHandler

[formatters]
keys=simpleFormatter

[logger_root]
level=INFO
;level = DEBUG
;level = ERROR
;handlers=consoleHandler
handlers=fileHandler

[handler_consoleHandler]
class=StreamHandler
formatter=simpleFormatter
args=(sys.stdout,)

[handler_fileHandler]
class=FileHandler
mode=
formatter=simpleFormatter
args=('/home/soumya/facegenerator/process.log', 'w')

[formatter_simpleFormatter]
format=%(asctime)s.%(msecs)03d: %(name)s %(levelname)s: File '%(filename)s', line %(lineno)s in %(funcName)s: %(message)s
datefmt=%y%m%d.%H%M%S
```


### config.ini
```ini
[DataGeneratorSR]
face_tr : /media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_
face_gt : /media/soumya/HDD_1/testing_subset/ground_truth
ignore: /media/soumya/HDD_1/face_clustering/Non_acceptable_images.txt
test_data_path: /home/soumya/PhD@AnyVision/deeplearning/data/SR_TEST_FACE

[DataGeneratorAR]
face_tr : /media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_LR/Q10
face_gt : /media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_
ignore: /media/soumya/HDD_1/face_clustering/Non_acceptable_images.txt
test_data_path: /media/soumya/HDD_1/testing_subset/SR_TEST_FACE_20
test_data_path_gt: /media/soumya/HDD_1/testing_subset/SR_TEST_FACE

[DataGeneratorARSR]
face_tr : /media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_LR/Q10
face_gt : /media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_
ignore: /media/soumya/HDD_1/face_clustering/Non_acceptable_images.txt
test_data_path: /media/soumya/HDD_1/testing_subset/SR_TEST_FACE_10
test_data_path_gt: /media/soumya/HDD_1/testing_subset/SR_TEST_FACE

[FaceRecognition]
face_gt : /media/soumya/HDD_1/testing_subset/ground_truth
face_data : /media/soumya/HDD_1/testing_subset/ms_anyv_umd_MiddleAsian_renren_

[main]
model_path : /home/soumya/facegenerator/
save_path : /home/soumya/facegenerator/
check_every : 200
batch_size : 8
vgg_model_path : /home/soumya/PhD@AnyVision/facegenerator/data/vgg19_model/latest
```


# Version
* Version 1 of the AR system.
* Generator-v1
* Discriminator-v3
* Loss-canny+vgg (ratio: 0.4)
* Channels 1
* Loss calculated with images (not logits).
* Might need to train again as one of them (generator output) was logits by mistake.
* Images and models [here](https://drive.google.com/open?id=1jhxKAxqZExbhOlL099t12TLuC0pBCkle)