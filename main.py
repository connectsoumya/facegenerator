import logging.config
import os
import sys
# import pickle
# import numpy as np
#
# import facegenerator.utils.dlib_face_recognition.api as face_recognition
# from facegenerator.utils.datagenerator import DataGenerator
from facegenerator.core.SRTCN_multiGPU import SuperResolution
from facegenerator.core.ARGAN_multiGPU import ArtifactRemoval

if sys.version_info.major == 2:
    from ConfigParser import SafeConfigParser

    parser = SafeConfigParser()
else:
    from configparser import ConfigParser

    parser = ConfigParser()

my_path = os.path.abspath(os.path.dirname(__file__))
log_file_path = os.path.join(my_path, 'logging.ini')
config_file_path = os.path.join(my_path, 'config.ini')
parser.read(config_file_path)
logging.config.fileConfig(log_file_path)

"""
Download models from https://github.com/ageitgey/face_recognition_models
Also available in anyvision google drive
Face recognition engine taken from https://github.com/ageitgey/face_recognition
"""

#
# def get_encodings():
#     """
#     Get the face encodings of the facial images.
#     :return:
#     """
#     face_encodings = []
#     for i in range(100):
#         try:
#             face_gt = face_recognition.load_image_file(gt_path_ + '/%05d/gt.jpg' % i)
#             try:
#                 face_gt_encoding = face_recognition.face_encodings(face_gt)[0]
#             except IndexError:
#                 face_gt_encoding = face_recognition.face_encodings(face_gt)
#             face_encodings.append(face_gt_encoding)
#         except IOError:
#             face_encodings.append(None)
#     with open('face_encodings.pkl', 'wb') as handle:
#         pickle.dump(face_encodings, handle, protocol=pickle.HIGHEST_PROTOCOL)
#
#
# def match_face():
#     """
#     Match the faces.
#     :return:
#     """
#     with open('face_encodings.pkl', 'rb') as handle:
#         face_gt_encodings = pickle.load(handle)
#     j = 0
#     for i in range(100):
#         face_query = face_recognition.load_image_file(data_path_ + '/%05d/%d.jpg' % (i, j))
#         face_query_encoding = face_recognition.face_encodings(face_query)[0]
#         results = face_recognition.compare_faces(np.concatenate(face_gt_encodings), face_query_encoding)


def train_SRTCN_multiGPU():
    tr_path_ = parser.get('DataGeneratorIMNETSR', 'tr')
    test_path = parser.get('DataGeneratorIMNETSR', 'test_data_path')
    channels = parser.get('DataGeneratorIMNETSR', 'channels')
    num_gpu = int(parser.get('main', 'num_GPU'))
    GPU = parser.get('main', 'GPU').split()
    img_save_path = parser.get('main', 'save_path')
    model_save_path = parser.get('main', 'model_path')
    assert num_gpu == len(GPU)
    batch_size_ = int(parser.get('main', 'batch_size'))
    check_every_ = int(parser.get('main', 'check_every'))
    kwargs = {'check_every': check_every_,
              'channels': channels,
              'img_save_path': img_save_path,
              'model_save_path': model_save_path,
              'test_path': test_path,
              'GPU_list' : GPU}
    sr = SuperResolution(batch_size=batch_size_, learning_rate=0.001, epoch=100, **kwargs)
    # sr.fit_graph(tr_path_)
    sr.predict(test_path)

def train_ARTCN_multiGPU():
    gt_path_ = parser.get('DataGeneratorIMNETAR', 'gt')
    tr_path_ = parser.get('DataGeneratorIMNETAR', 'tr')
    test_path = parser.get('DataGeneratorIMNETAR', 'test_data_path')
    test_path_gt = parser.get('DataGeneratorIMNETAR', 'test_data_path_gt')
    channels = parser.get('DataGeneratorIMNETSR', 'channels')
    img_save_path = parser.get('main', 'save_path')
    model_save_path = parser.get('main', 'model_path')
    GPU = parser.get('main', 'GPU').split()
    batch_size_ = int(parser.get('main', 'batch_size'))
    check_every_ = int(parser.get('main', 'check_every'))
    kwargs = {'check_every': check_every_,
              'channels': channels,
              'img_save_path': img_save_path,
              'model_save_path': model_save_path,
              'test_path': test_path,
              'GPU_list': GPU}
    f = ArtifactRemoval(batch_size=batch_size_, learning_rate=0.0001, epoch=100, **kwargs)
    # f.fit_graph(tr_path_)
    f.predict(test_path)


if __name__ == '__main__':
    train_ARTCN_multiGPU()
