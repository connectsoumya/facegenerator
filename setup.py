from setuptools import setup, Command
import os
import sys
from os.path import dirname
from os.path import abspath
import errno


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('rm -vrf ./build ./dist ./*.pyc ./*.tgz ./*.egg-info')
        os.rmdir(os.path.expanduser('~') + '/facegenerator/')


if sys.argv[-1] == 'install':
    if sys.version_info.major == 2:
        from ConfigParser import SafeConfigParser

        parser = SafeConfigParser()
    else:
        from configparser import ConfigParser

        parser = ConfigParser()
    homedir = os.path.expanduser('~')
    try:
        os.makedirs(homedir + '/facegenerator/')
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
    parser.read('logging.ini')
    parser.set('handler_fileHandler', 'args', homedir + '/facegenerator/')
    # Writing our configuration file to 'example.ini'
    with open('logging.ini', 'wb') as configfile:
        parser.write(configfile)

setup(name="facegenerator",
      version="0.1",
      description="Frontal face pose generation.",
      author="Soumya Shubhra Ghosh",
      author_email="soumya.gre@gmail.com",
      url="anyvision.co",
      install_requires=[
          'numpy>=1.13.3',
          'scipy>=0.19.1',
          'scikit-learn>=0.18.1',
          'scikit-image>=0.13.0',
          'cython>=0.25.2',
          'dlib>=19.10.0',
          'configparser>=3.5.0',
          'pillow>=4.1.1',
          'configparser>=3.5.0',
          'matplotlib>=2.0.0',
          'tensorflow-gpu>=1.3.0',
          'tensorlayer>=1.8.4',
          'tqdm>=4.11.2',
          'tensorflow', 'skimage',
      ],
      packages=['facegenerator',
                'facegenerator/core',
                'facegenerator/utils', ],
      package_data={},
      include_package_data=True,
      long_description="""This package contains the training model for generating frontal face pose.""",

      classifiers=['Development Status :: 3 - Alpha',
                   'Intended Audience :: Developers',
                   'Topic :: Software Development :: Build Tools',
                   'Operating System :: POSIX :: Linux',
                   'Programming Language :: Python',
                   'License :: OSI Approved :: MIT License',
                   'Programming Language :: Python :: 2.7'],

      cmdclass={
          'clean': CleanCommand,
      }
      )
